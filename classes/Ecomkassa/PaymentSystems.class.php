<?php

namespace Ecomkassa;

/* Платежные системы */

class PaymentSystems
{

    const LIMIT = 100;

	public function __construct($db, $user_id = null, $psid = null)
	{
        $this->db = is_null($db) ? new \Ecomkassa\DB() : $db;
        $this->user_id = $user_id;
        $this->psid = $psid;
        $this->allow = ["tinkoff"];
    }

    public function get() {

        $select = $this->db->query('SELECT id, vendor, tokens, created FROM payment_systems WHERE id = ? and user_id = ? LIMIT 1', [$this->psid, $this->user_id]);

		if ($select->numRows() == 0)
            return [
                "error" => true,
                "message" => "Платежная система не найдена",
            ];

        $ps = $select->fetchArray();
        $ps["tokens"] = json_decode($ps["tokens"], true);

        return $ps;
        
    }

    public function list() {
        $page = (isset($_GET["page"]) && intval($_GET["page"]) > 1) ? intval($_GET["page"]) : 1;
        $select = $this->db->query('SELECT id, vendor, tokens, created FROM payment_systems WHERE user_id = ? ORDER BY id DESC LIMIT ? OFFSET ?', [$this->user_id, self::LIMIT, ($page - 1) * self::LIMIT]);

        $count = $select->numRows();
        $results = $select->fetchAll();

        foreach ($results as $key=>$child) {
            $results[$key]['tokens'] = json_decode($results[$key]["tokens"], true);
            unset($results[$key]["data_raw"]);
        }

        return [
            "_links" => [
                "next" => $count == self::LIMIT ? "/api/v1/payment_systems?page=" . ($page + 1) : null
            ],
            "limit" => self::LIMIT,
            "results" => $results
        ];
    }


    public function add($data) {

        $check = $this->check($data);

        if (isset($check['error']))
            return $check;

        if ($data["vendor"] == "tinkoff") {

            $Tinkoff = new \Vendors\Tinkoff($this->db);
            $creditionals = $Tinkoff->check($data['tokens']);

            if (isset($creditionals['error']))
                return $creditionals;

        }

        $insert = $this->db->query('INSERT INTO payment_systems (user_id, vendor, tokens) VALUES (?,?,?)', [
            $this->user_id,
            $data["vendor"],
            json_encode($data['tokens'], JSON_UNESCAPED_UNICODE),
        ]);

        if ($insert->affectedRows() == 0)
            return [
                "error" => true,
                "message" => "Ошибка добавления, повторите позже"
            ];

        return [
            "id" => $insert->lastInsertId()
        ];

    }

    public function update($data) {

        $select = $this->db->query('SELECT id FROM payment_systems WHERE id = ? and user_id = ? LIMIT 1', [$this->psid, $this->user_id]);

		if ($select->numRows() == 0)
            return [
                "error" => true,
                "message" => "Платежная система не найдена",

            ];
		

        $check = $this->check($data);

        if (isset($check['error']))
            return $check;

        if ($data["vendor"] == "tinkoff") {

            $Tinkoff = new \Vendors\Tinkoff($this->db);
            $creditionals = $Tinkoff->check($data['tokens']);

            if (isset($creditionals['error']))
                return $creditionals;

        }

        $update = $this->db->query('UPDATE payment_systems SET vendor = ?, tokens = ? WHERE id = ? and user_id = ?', [
            $data["vendor"],
            json_encode($data['tokens'], JSON_UNESCAPED_UNICODE),
            $this->psid,
            $this->user_id
        ]);

        return array_merge(["id" => $this->psid], $data);

    }

    public function check($data) {
        if (!isset($data["vendor"]))
            return [
                "error" => true,
                "message" => "Не указан вендор платежной системы"
            ];

        if (!in_array($data["vendor"], $this->allow))
            return [
                "error" => true,
                "message" => "Вендор платежной системы не поддерживается"
            ];

        if (!isset($data["tokens"]))
            return [
                "error" => true,
                "message" => "Не переданы учетные данные (tokens)"
            ];

        return [];
    }


}