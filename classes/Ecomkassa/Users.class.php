<?php

namespace Ecomkassa;

/* Пользователи */

class Users
{

	
	public function __construct($db = null)
	{
        $this->db = is_null($db) ? new \Ecomkassa\DB() : $db;
    }

    public function registration($data) 
	{

        if (!isset($data["inn"]))
            return [
                "error" => true,
                "message" => "Не указан ИНН"
            ];

        if (!$this->isValidINN($data["inn"]))
            return [
                "error" => true,
                "message" => "ИНН указан неверно"
            ];

		$select = $this->db->query('SELECT id FROM users WHERE inn = ? LIMIT 1', [$data["inn"]]);

		if ($select->numRows() != 0)
            return [
                "error" => true,
                "message" => "Пользователь существует с ИНН " . $data["inn"]
            ];
		
        $insert = $this->db->query('INSERT INTO users (inn) VALUES (?)', [$data["inn"]]);
        if ($insert->affectedRows() == 0)
            return [
                "error" => true,
                "message" => "Ошибка регистрации, повторите позже"
            ];

        $session = $this->createSession($insert->lastInsertId());

        if (isset($session["error"]))
            return $session;

        return [
            "token" => $session["token"]
        ];

    }

    private function createSession($user_id) {
        $token = $this->randomHash(1024);
        $insert = $this->db->query('INSERT INTO sessions (user_id, token, device) VALUES (?,?,?)', [$user_id, $token, $_SERVER['HTTP_USER_AGENT']]);
        if ($insert->affectedRows() == 0)
            return [
                "error" => true,
                "message" => "Ошибка создания токена, повторите авторизацию позже"
            ];

        return [
            "token" => $token
        ];
    }

    public function auth($data) 
	{

        if (!isset($data["inn"]))
            return [
                "error" => true,
                "message" => "Не указан ИНН"
            ];

        if (!$this->isValidINN($data["inn"]))
            return [
                "error" => true,
                "message" => "ИНН указан неверно"
            ];

		$select = $this->db->query('SELECT id FROM users WHERE inn = ? LIMIT 1', [$data["inn"]]);

		if ($select->numRows() == 0)
            return [
                "error" => true,
                "message" => "Пользователь не найден"
            ];

        $session = $this->createSession($select->fetchArray()["id"]);

        if (isset($session["error"]))
            return $session;

        return [
            "token" => $session["token"]
        ];

    }
		

    public function isValidINN($inn): bool
    {

        if (preg_match('/\D/', $inn)) {
            return false;
        }

        $len = strlen($inn);

        if ($len === 10) {
            $num9 = (string) (((2 * (int) $inn[0] + 4 * (int) $inn[1] + 10 * (int) $inn[2] + 3 * (int) $inn[3] + 5 * (int) $inn[4] + 9 * (int) $inn[5] + 4 * (int) $inn[6] + 6 * (int) $inn[7] + 8 * (int) $inn[8]) % 11) % 10);

            return $inn[9] === $num9;
        } elseif ($len === 12) {
            $num10 = (string) (((7 * (int) $inn[0] + 2 * (int) $inn[1] + 4 * (int) $inn[2] + 10 * (int) $inn[3] + 3 * (int) $inn[4] + 5 * (int) $inn[5] + 9 * (int) $inn[6] + 4 * (int) $inn[7] + 6 * (int) $inn[8] + 8 * (int) $inn[9]) % 11) % 10);

            $num11 = (string) (((3 * (int) $inn[0] + 7 * (int) $inn[1] + 2 * (int) $inn[2] + 4 * (int) $inn[3] + 10 * (int) $inn[4] + 3 * (int) $inn[5] + 5 * (int) $inn[6] + 9 * (int) $inn[7] + 4 * (int) $inn[8] + 6 * (int) $inn[9] + 8 * (int) $inn[10]) % 11) % 10);

            return $inn[11] === $num11 && $inn[10] === $num10;
        }

        return false;
    }

    public function getUserByToken($token) {
        $select = $this->db->query('SELECT user_id FROM sessions WHERE token = ? LIMIT 1', $token);
        if ($select->numRows() == 1)
            return $select->fetchArray();
        else 
            return false;
    }

    public function randomHash(
        $length = 128
    )
    {
        $hash = '';
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $limit = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $characters[rand(0, $limit)];
        }
        return $hash;
    }

}
	