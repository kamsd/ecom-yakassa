<?php

namespace Ecomkassa;

/* Покупатели */

class Customers
{

    const LIMIT = 100;
	
	public function __construct($db = null, $user_id = null)
	{
        $this->db = is_null($db) ? new \Ecomkassa\DB() : $db;
        $this->user_id = $user_id;
    }

    public function add($contacts) {

        if (!isset($contacts["phone"]) && !isset($contacts["email"]))
            return [
                "error" => true,
                "message" => "Не указан phone и email"
            ];

        $find = $this->find($contacts);

        if (count($find) > 0)
            return $find;

       
        $insert = $this->db->query('INSERT INTO customers (phone, email, user_id) VALUES (?,?,?)', [$contacts["phone"] ?? null, $contacts["email"] ?? null, $this->user_id]);
        if ($insert->affectedRows() == 0)
            return [
                "error" => true,
                "message" => "Ошибка создания токена, повторите авторизацию позже"
            ];

        return [
            "id" => $insert->lastInsertId()
        ];
    }

    public function findById($id) {
        return $this->db->query('SELECT id FROM customers WHERE id = ? and user_id = ? LIMIT 1', [$id, $this->user_id])->fetchArray();
    }

    public function find($contacts) {
        return $this->db->query('SELECT id FROM customers WHERE (phone = ? or email = ?) and user_id = ? LIMIT 1', [$contacts["phone"], $contacts["email"], $this->user_id])->fetchArray();
    }

    public function list() {
        $page = (isset($_GET["page"]) && intval($_GET["page"]) > 1) ? intval($_GET["page"]) : 1;
        $select = $this->db->query('SELECT id, email, created FROM customers WHERE user_id = ? ORDER BY id DESC LIMIT ? OFFSET ?', [$this->user_id, self::LIMIT, ($page - 1) * self::LIMIT]);
        return [
            "_links" => [
                "next" => $select->numRows() == self::LIMIT ? "/api/v1/customers?page=" . ($page + 1) : null
            ],
            "limit" => self::LIMIT,
            "results" => $select->fetchAll()
        ];
    }

}