<?php

namespace Ecomkassa;

/* Платежи */

class Payments
{

    /* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
    }
	
	/* Получение информации о платеже */
	public function getInfo($uid) 
	{
		$return = array();

		/* Отправляем запрос в БД */
		$select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $uid);

		if ($select->numRows() == 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж не найден';
            return $return;
		}
		
		$order = $select->fetchArray();

		$order['data_raw'] = json_decode($order['data_raw'], true);
		$order['data_callback'] = json_decode($order['data_callback'], true);

		if ($order['source'] == 'yandex') {
			$response = (new \Vendors\Yandex())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'tinkoff') {
			$response = (new \Vendors\Tinkoff())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'sberbank') {
			$response = (new \Vendors\Sberbank())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'rbkmoney') {
			$response = (new \Vendors\RBKMoney())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'invoice') {
			$response = (new \Vendors\Invoice())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'ecomkassa') {
			$response = (new \Vendors\Invoice())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'tochka') {
			$response = (new \Vendors\Tochka())->payment_check($order['uid'], $order['data_raw']);
			return $response;
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		} elseif ($order['source'] == 'yourpayments_podeli') {
			$response = (new \Vendors\YourPaymentsPodeli())->payment_check($order['uid'], $order['data_raw']);
			if (!array_key_exists('error',$response)) {
				return $response;
			}
		}

		return array(
			'status' => $order['status'],
			'updated' => $order['updated'],
		);
    }
    
}
