<?php

namespace Ecomkassa;

/* Рекурентные платежи */

class Autopayments
{

    const LIMIT = 100;

	public function __construct($db, $user_id = null)
	{
        $this->db = is_null($db) ? new \Ecomkassa\DB() : $db;
        $this->user_id = $user_id;
    }


    public function list() {
        $page = (isset($_GET["page"]) && intval($_GET["page"]) > 1) ? intval($_GET["page"]) : 1;
        $select = $this->db->query('SELECT id, uid, external_uid, source, customer_id, payment_system, status, created, updated FROM autopayments WHERE user_id = ? and recurrent = ? ORDER BY id DESC LIMIT ? OFFSET ?', [$this->user_id, 1, self::LIMIT, ($page - 1) * self::LIMIT]);
        return [
            "_links" => [
                "next" => $select->numRows() == self::LIMIT ? "/api/v1/autopayments?page=" . ($page + 1) : null
            ],
            "limit" => self::LIMIT,
            "results" => $select->fetchAll()
        ];
    }

    public function get($id) {
        $select = $this->db->query('SELECT id, uid, external_uid, source, customer_id, payment_system, data_raw, rebillId, status, created, updated FROM autopayments WHERE id = ? and user_id = ? and recurrent = ? ORDER BY id DESC LIMIT 1', [$id, $this->user_id, 1]);
        if ($select->numRows() == 0) {
            $select = $this->db->query('SELECT id, uid, external_uid, source, customer_id, payment_system, data_raw, rebillId, status, created, updated FROM autopayments WHERE id = ? and user_id = ? and recurrent = ? ORDER BY id DESC LIMIT 1', [$id, $this->user_id, 0]);
            if ($select->numRows() == 0)
                return [
                    'error' => [
                        'code' => 404,
                        'message' => "Платеж не найден"
                    ]
                ];

            $payment = $select->fetchArray();
            $payment["details"] = json_decode($payment["data_raw"], true)["payment"];
            unset($payment["rebillId"]);
            unset($payment["data_raw"]);

            return $payment;
        }



        $payment = $select->fetchArray();

        $payment["details"] = json_decode($payment["data_raw"], true)["payment"];

        $payment['children'] = $this->db->query('SELECT id, uid, external_uid, status, data_raw, created, updated FROM autopayments WHERE user_id = ? and recurrent = ? and rebillId = ? ORDER BY id DESC', [$this->user_id, 0, $payment["rebillId"]])->fetchAll();
        
        foreach ($payment['children'] as $key=>$child) {
            $payment['children'][$key]['details'] = json_decode($payment['children'][$key]["data_raw"], true)["payment"];
            unset($payment['children'][$key]["data_raw"]);
        }
        unset($payment["rebillId"]);
        unset($payment["data_raw"]);

        return $payment;
    }


}