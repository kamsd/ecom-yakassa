<?php

namespace Ecomkassa;

/* Класс для работы с онлайн-кассой Ecomkassa */

class Kassa
{
	
	public function __construct()
	{

        $this->base_url = "https://app.ecomkassa.ru";

        $this->login = null;
        $this->pass = null;
        $this->inn = null;
        $this->store_id = null;
        $this->payment_type = null;

        $this->token = NULL;
        $this->vat = "none";
        $this->sno = "usn_income";
        
    }


    private function getToken() {
        $curl = curl_init($this->base_url . '/fiscalorder/v4/getToken?login='.$this->login.'&pass='.$this->pass);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        json_decode($result);
        if (json_last_error() == JSON_ERROR_NONE) {
            $return = json_decode($result, true);
            if (isset($return['token'])) {
                $this->token = $return['token'];
                return $return['token'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function api($url, $data = false) {

        $this->getToken();

        $ch = curl_init($this->base_url . $url . '?token='.$this->token);

        $headers = ['Content-Type: application/json'];
        if ($data) {
            $data_string = json_encode($data);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
        }
        if (!is_null($this->token)) 
            $headers[] = 'token: '.$this->token;

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        return json_decode($result, true);
    }

    public function check($order) {

        $kassa = [];
        $kassa['external_id'] = $order['order_id'];
      
        $receipt = [];

        $client = new \stdClass();
        $client->email = $order['email'];

        $receipt['client'] = $client;

        $receipt['prePaid'] = true;

        $company = new \stdClass();
        $company->sno = $order['Receipt']['taxation'];
        $company->inn = $this->inn;
        $company->payment_address = "";

        $receipt['company'] = $company;
      
        $vat = new \stdClass();
        $vat->type = $this->vat;

        ini_set('precision', 5);

        foreach ($order['Receipt']['items'] as $item) {

            $v = [
                "name" => $item['name'],
                "price" => $item['price'],
                "quantity" => (float) $item['quantity'],
                "sum" => $item['sum']
            ];

            if (isset($item["payment_method"]))
                $v["payment_method"] = $item['payment_method'];

            if (isset($item["payment_object"]))
                $v["payment_object"] = $item['payment_object'];

            if (isset($item["tax"])) {
                $tax = new \stdClass();
                $tax->type = $item["tax"];
                $v["vat"] = $tax;
            }
                

            $receipt['items'][] = $v;
        }
      
        $payments = [];
        $payments[] = [
            "type" => $this->payment_type,
            "sum" => $order['amount']
        ];
        
        $receipt['payments'] = $payments;
      
        $receipt['total'] = $order['amount'];
      
        $kassa['receipt'] = $receipt;
    
        $kassa['timestamp'] = date('d.m.Y H:i:s');

        if (isset($order['service']))
        {
            $kassa['service'] = $order['service'];
        }

        //return $kassa;

        //return $response = $this->api('/fiscalorder/v2/'.$this->store_id.'/paymentTypes');

        $response = $this->api('/fiscalorder/v2/'.$this->store_id.'/sell', $kassa);
        
        if (!isset($check['invoice_payload'])) {
            $response["payload"] = $kassa;

        }

        return $response;

    }



}