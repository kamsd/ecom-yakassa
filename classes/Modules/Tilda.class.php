<?php

namespace Modules;

class Tilda
{
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
    }

    public function getLink($inputStr) {

        /*
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://webhook.site/8e2f08e6-590f-466d-88c4-14e1a8e0582e',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>$inputStr,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: text/plain'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        */

        parse_str($inputStr, $output);
        $data = [];
        foreach ($output as $key=>$value) {
            $data[$key] = urldecode($value);
            if (in_array($key,["cart", "Receipt", "cart"])) {
                $data[$key] = json_decode($data[$key], true);
            }
        }
        $settings = explode("|", $data["login"]);

        if (count($settings) != 5) {
            return [
                "error" => true,
                "message" => "Логин указан некорректно"
            ];
        }

        $Kassa = new \Ecomkassa\Kassa();

        $Kassa->login = $settings[0];
        $Kassa->pass = $settings[1];
        $Kassa->inn = $settings[2];
        $Kassa->store_id = $settings[3];
        $Kassa->payment_type = (int)$settings[4];

        if (isset($data['webhook_url'])) {
            $data['service'] = [
                'callback_url' => $data["webhook_url"],
                'extra_callback_urls' => ["https://env-5581354.mircloud.ru/api/v1/webhooks/tilda", "https://webhook.site/8e2f08e6-590f-466d-88c4-14e1a8e0582e"]
            ];
        }
            
        $check = $Kassa->check($data);
    
        if (isset($check['invoice_payload']) && isset($check['invoice_payload']['link'])) {

            $insert = $this->db->query('INSERT INTO tilda_orders (order_id,	uuid, amount) VALUES (?,?,?)', [$data['order_id'], $check['uuid'], $data['amount']]);

            return [
                "link" => $check['invoice_payload']['link']
            ];
            
        } else {
            return [
                "error" => true,
                "message" => "Не удалось получить ссылку на оплату",
                "payload" => $check,
                "data" => $data
            ];
        }
    }

    public function webhook_insert($body)
    {
        if ( isset($body['uuid']) && isset($body['callback_url']) && isset($body['status']) && $body['status'] == "done" ) {
            $this->sendSuccess($body['callback_url'], $body['uuid']);
        }
    }

    public function sendSuccess($url, $uuid) {

        $data = $this->db->query('SELECT order_id, amount FROM tilda_orders WHERE uuid = ?', $uuid)->fetchArray();
 
        if (count($data) == 0) return null;

        $data["status"] = "success";
        $data["sign"] = md5("ecomkassa|".$data['amount']."|".$data['order_id']."|".$data["status"]);
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data
        ));
    
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}


    