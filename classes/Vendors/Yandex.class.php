<?php

namespace Vendors;

/* Касса пользователя */

class Yandex
{
	
	/* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
        $this->shopId = false;
        $this->secretKey = false;

        $this->client = new \YandexCheckout\Client(); // инициализация Yandex.Checkout API PHP Client Library
    }


    public function webhook_insert($headers, $body)
    {
        

        if ( (array_key_exists('object', $body)) and (array_key_exists('id', $body['object'])) ) {
            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($body), $body['object']['id']));

            /* Добавляем запись в БД */
            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('yandex',json_encode($headers), json_encode($body)) );
            $num_rows = $insert->affectedRows();

        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Yandex Webhooks отклонен';
            return $return;
        }
        
        if ($num_rows == 1) {

            if (in_array($body['event'], array('payment.waiting_for_capture', 'payment.canceled', 'payment.succeeded'))) {

                $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $body['object']['id']);

                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Yandex Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);

            }

            if ($body['event'] == 'payment.waiting_for_capture') {

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array(1, $body['object']['id']));

                //$this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 1, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 1, 'time' => date('Y-m-d H:i:s')));

                if (array_key_exists('OAuthToken', $data['tokens'])) {
                    $this->client->setAuthToken($data['tokens']['OAuthToken']);
                } elseif (array_key_exists('shopId', $data['tokens'])) {
                    $this->client->setAuth($data['tokens']['shopId'], $data['tokens']['secretKey']);
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Учетные данные (ключ) для Яндекс.Касса не указаны';
                    return $return;
                }

                $paymentId = $body['object']['id'];
                $idempotenceKey = uniqid('', true);
                $response = $this->client->capturePayment(
                    array(
                        'amount' => $body['object']['amount']
                    ),
                    $paymentId,
                    $idempotenceKey
                );

            } elseif ($body['event'] == 'payment.canceled') {

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array(3, $body['object']['id']));

                //$this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 3, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 3, 'time' => date('Y-m-d H:i:s')));
            
            } elseif ($body['event'] == 'payment.succeeded') {

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array(2, $body['object']['id']));

                //$this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 2, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => 2, 'time' => date('Y-m-d H:i:s')));
            
            } else {

                $return['message'] = 'Тип события Yandex Webhooks не поддерживается, запрос отклонен';
                return $return;
            }

            $return['success'] = true;
            $return['message'] = 'Yandex Webhooks успешно обработан';
            return $return;

        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД (Yandex Webhooks)';
            return $return;
        }  
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ?', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        if (array_key_exists('OAuthToken', $data['tokens'])) {
            $this->client->setAuthToken($data['tokens']['OAuthToken']);
        } elseif (array_key_exists('shopId', $data['tokens'])) {
            $this->client->setAuth($data['tokens']['shopId'], $data['tokens']['secretKey']);
        } else {
            $return['error'] = true;
            $return['message'] = 'Учетные данные (ключ) для Яндекс.Касса не указаны';
            return $return;
        }

        $idempotenceKey = uniqid('', true);

        try {
            $response = $this->client->createPayment(
                array(
                    'amount' => array(
                        'value' => $data['payment']['amount'],
                        'currency' => $data['payment']['currency']
                    ),
                    'confirmation' => array(
                        'type' => 'redirect',
                        'return_url' => $data['return_url']
                    ),
                    'description' => $data['payment']['description']
                ),
                $idempotenceKey
            );

        } catch (\Exception $e) {
            $return['error'] = true;
            $return['message'] = 'Ошибка при создании платежа через Яндекс.Кассу: '.$e->getMessage();
            return $return;
        }

        $this->setWebhooks();

        $sql_vars = array(
            $response['id'],
            $data['uid'],
            'yandex',
            json_encode($data),
            json_encode($response),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);
        if ($insert->affectedRows() == 1) {
            return array(
                'payment_url' => $response['confirmation']['confirmation_url']
            );
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }

    public function payment_check($paymentId, $data) {

        if (array_key_exists('OAuthToken', $data['tokens'])) {
            $this->client->setAuthToken($data['tokens']['OAuthToken']);
        } elseif (array_key_exists('shopId', $data['tokens'])) {
            $this->client->setAuth($data['tokens']['shopId'], $data['tokens']['secretKey']);
        } else {
            $return['error'] = true;
            $return['message'] = 'Учетные данные (ключ) для Яндекс.Касса не указаны';
            return $return;
        }

        try {
            $response = $this->client->getPaymentInfo($paymentId);

            $status = 0;

            if ($response['status'] == 'waiting_for_capture') {
                $status = 1;
            } elseif ($response['status'] == 'succeeded') {
                $status = 2;
            } elseif ($response['status'] == 'canceled') {
                $status = 4;
            } else {

                $return['message'] = 'Тип события не поддерживается, запрос отклонен';
                return $return;
            }

            $this->db->query('UPDATE orders SET status = ?, data_callback = ? WHERE uid = ?', array($status, json_encode($response), $response['id']));

            return array(
                'status' => $status,
                'updated' => date('Y-m-d H:i:s')
            );


        } catch (\Exception $e) {
            $return['error'] = true;
            $return['message'] = $e->getMessage();
            return $return;
        }

    }

    
    private function check_payment_data($data) {

        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('currency', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует валюта платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к Яндекс.Касса';
            return $return;
        }

        if ( ( (!array_key_exists('shopId', $data['tokens'])) and (!array_key_exists('secretKey', $data['tokens'])) ) and (!array_key_exists('OAuthToken', $data['tokens'])) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к Яндекс.Касса';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }


        return array();

    }

    private function setWebhooks()
    {
        try {
            $this->client->addWebhook([
                "event" => \YandexCheckout\Model\NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE,
                "url"   => YA_WEBHOOK,
            ]);
        } catch (\Exception $e) { }

        try {
            $this->client->addWebhook([
                "event" => \YandexCheckout\Model\NotificationEventType::PAYMENT_SUCCEEDED,
                "url"   => YA_WEBHOOK,
            ]);
        } catch (\Exception $e) { }

        try {
            $this->client->addWebhook([
                "event" => \YandexCheckout\Model\NotificationEventType::PAYMENT_CANCELED,
                "url"   => YA_WEBHOOK,
            ]);
        } catch (\Exception $e) { }

        try {
            $this->client->addWebhook([
                "event" => \YandexCheckout\Model\NotificationEventType::REFUND_SUCCEEDED,
                "url"   => YA_WEBHOOK,
            ]);
        } catch (\Exception $e) { }
    }


    public function check($data)
    {

        if (array_key_exists('OAuthToken', $data)) {
            $this->client->setAuthToken($data['OAuthToken']);

            try {
                $response = $this->client->me();
                $return['message'] = 'Ключ доступа OAuthToken к Яндекс.Кассе действителен';
                return $return;
    
            } catch (\Exception $e) {
                $return['error'] = true;
                $return['message'] = $e->getMessage();
                return $return;
            }

        } elseif (array_key_exists('shopId', $data)) {

            $this->client->setAuth($data['shopId'], $data['secretKey']);

            $idempotenceKey = uniqid('', true);

            try {
                $response = $this->client->createPayment(
                    array(
                        'amount' => array(
                            'value' => '0.01',
                            'currency' => 'RUB'
                        ),
                        'confirmation' => array(
                            'type' => 'redirect',
                            'return_url' => 'https://app.ecomkassa.ru/'
                        ),
                        'description' => 'Test',
                        'test' => true
                    ),
                    $idempotenceKey
                );

                $return['message'] = 'Значения shopId и secretKey для доступа к Яндекс.Кассе действительны';
                return $return;
    
            } catch (\Exception $e) {
                $return['error'] = true;
                $return['message'] = $e->getMessage();
                return $return;
            }

        } else {
            $return['error'] = true;
            $return['message'] = 'Учетные данные (ключ) для Яндекс.Касса не указаны';
            return $return;
        }

        

    }


    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
	}

}
