<?php

namespace Vendors;

/* Интернет-эквайринг Invoice */

class Invoice
{
	
	/* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
        $this->login = false;
        $this->apikey = false;
    }


    public function webhook_insert($headers, $body)
    {
    
        if ((is_array($body)) and (isset($body['id']))) {

            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($body), $body['id']));

            /* Добавляем запись в БД */

            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('invoice',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($body)) );
            $num_rows = $insert->affectedRows();

        
        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Invoice отклонен';
            return $return;
        }

        if ($num_rows == 1) {

            if (array_key_exists('status', $body)) {

                $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $body['id']);
  
                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Invoice Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);

                if ( ($body['status'] == 'successful') ) {
                    $status = 2;
                } else {
                    $return['message'] = 'Тип события Invoice не поддерживается, запрос отклонен';
                    return $return;
                }

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $body['id']));
                //$this->redirect($data['callback_url'], array('uid' => $body['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $body['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                
            }                    

        }
        
        
 
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $response = array(
            'order' => array(
                'currency' => 'RUB',
                'amount' => round(floatval($data['payment']['amount']), 2),
                'description' => $data['payment']['description'],
                'id' => $data['uid']
            ),
            'settings' => array(
                'terminal_id' => $data['tokens']['terminal_id'],
                'payment_method' => 'card',
                'success_url' => $data['return_url'],
                'fail_url' => $data['return_url']
            ),
            'custom_parameters' => '',
            'receipt' => array(),
            'trtype' => 1
        );

        $output = $this->api("/CreatePayment", $response);

        if ((isset($output['payment_url'])) and (!isset($output['error']))) {

                $sql_vars = array(
                    $output['id'],
                    $data['uid'],
                    'invoice',
                    json_encode($data),
                    json_encode($output),
                    0
                );

                $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);

                if ($insert->affectedRows() == 1) {
                    
                    return array(
                        'payment_url' => $output['payment_url']
                    );
                }
                else  {
                    $return['error'] = true;
                    $return['message'] = 'Произошла ошибка при записи данных в БД';
                    return $return;
                } 
            
    
    
            return $response;

        } elseif (isset($output['error'])) {

            $return['error'] = true;
            $return['message']= $output['description'];
            return $return;

        } else {

            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при формировании короткой ссылки';
            return $return;

        }


    }

    public function payment_check($paymentId, $data_raw) {
        
        $this->token = $data_raw['tokens']['token'];
        
        $response = $this->api("/GetPaymentByOrder", $data);

        if ((is_array($response)) and (array_key_exists('status', $response))) {
            if (($response['status'] == 'paid') or ($response['status'] == 'fulfilled')) {
                $status = 2;
            } elseif ($response['status'] == 'cancelled')  {
                $status = 3;
            } elseif ($response['status'] == 'unpaid')  {
                $status = 1;
            } else {
                $status = 0;
            }

            $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['id']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $select = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $response['id']);
                    $order = $select->fetchArray();

                    return array(
                        'status' => $order['status'],
                        'updated' => $order['updated']
                    );
                }

        } else {
            $return['error'] = true;
            $return['message'] = 'Неизвестная ошибка';
            return $return;
        }

        $return = array();

        return $response;

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют учетные данные tokens для подключения к терминалу интернет-эквайринга Invoice';
            return $return;
        }

        if (!array_key_exists('login', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует login для подключения к терминалу интернет-эквайринга Invoice';
            return $return;
        }

        if (!array_key_exists('apikey', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует API-ключ для подключения к терминалу интернет-эквайринга Invoice';
            return $return;
        }

        if (!array_key_exists('terminal_id', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Не указан идентификатор магазина terminal_id';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    
    private function api($method,$data = false){
                                                                                                                                                                                                           
        $ch = curl_init(INVOICE_BASE_URL.$method);

        if ($data) {
            $data_string = json_encode($data);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        }                                                               
                                                                      
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->login . ":" . $this->apikey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json; charset=utf-8'                                                  
        ));

        $result = curl_exec($ch);

        return json_decode($result, true);
    }

    public function check($data){

        if (!array_key_exists('login', $data)) {
            $return['error'] = true;
            $return['message'] = 'Не указан login для подключения к терминалу интернет-эквайринга Invoice';
            return $return;
        }

        if (!array_key_exists('apikey', $data)) {
            $return['error'] = true;
            $return['message'] = 'Не указан API-ключ для подключения к терминалу интернет-эквайринга Invoice';
            return $return;
        }

        $this->login = $data['login'];
        $this->apikey = $data['apikey'];

        $response = $this->api(
            "/GetTerminal",
            array(
                "id" => "00000"
            )
        );

        $return = array();

        if ( (is_array($response)) and (isset($response['error'])) ) {

            if ((int)$response['error'] == 3){
                $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга Invoice действительны';
            } else {
                $return['error'] = true;
                $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга Invoice не действительны';
            }

        } else {
            $return['error'] = true;
            $return['message'] = 'Шлюз интернет-эквайринга Invoice не доступен, попробуйте позже'; 
        }

        return $return; 
     
    }

    private function genUID() {
        $uid = uniqid(NULL, TRUE);
        $rawid = strtoupper(sha1(uniqid(rand(), true)));
        $result = substr($uid, 6, 8);
        $result .= substr($uid, 0, 4);
        $result .= substr(sha1(substr($uid, 3, 3)), 0, 4);
        $result .= substr(sha1(substr(time(), 3, 4)), 0, 4);
        $result .= strtolower(substr($rawid, 10, 12));
        return $result;
    }

}


