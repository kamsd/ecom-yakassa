<?php

namespace Vendors;

/* Интернет-эквайринг АлфаБанк */

class Alfabank
{

    //const API_BASE_URL = "https://web.rbsuat.com/ab/rest";

    const API_BASE_URL_NEW = "https://payment.alfabank.ru/payment/rest";
    const API_BASE_URL = "https://pay.alfabank.ru/payment/rest";
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
        $this->login = false;
        $this->pass = false;
        $this->base_url = null;
    }



    public function check($data) {

        $return = array();

        if ( (!array_key_exists('userName', $data)) and (!array_key_exists('password', $data)) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют учетные данные для подключения к терминалу интернет-эквайринга Альфабанк';
            return $return;
        }


        if (mb_substr($data['userName'], 0, 2) == "r-")
                $this->base_url = self::API_BASE_URL_NEW;
        else
            $this->base_url = self::API_BASE_URL;

        $url = $this->base_url . '/register.do';

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query(
                    array(
                        'userName' => $data['userName'],
                        'password' => $data['password'],
                        'orderId' => '00000'
                    )
                )
            ),
            'ssl' => array(
                "verify_peer" => false,
                "verify_peer_name" => false
            )
        );

        $context  = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);


        if ($result === FALSE) {
            $return['error'] = true;
            $return['message'] = 'Сервер интернет-эквайринга АльфаБанк не доступен';
            return $return;
        }

        $result = json_decode($result, true);
        
        if ( (array_key_exists('errorCode', $result)) and ($result['errorCode'] != 5) ) {
            $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга АльфаБанк действительны';
            return $return;
        } else {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга АльфаБанк не действительны: '.$result['errorMessage'];
            return $return;
        }   

    }


    public function webhook_insert($headers, $get)
    {
        
        if (array_key_exists('mdOrder', $get)) {

            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($get, JSON_UNESCAPED_UNICODE), $get['mdOrder']));

            /* Добавляем запись в БД */
            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('alfabank',json_encode($headers), json_encode($get)) );
            $num_rows = $insert->affectedRows();

        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Alfabank Webhooks отклонен';
            return $return;
        }
        
        
        if ($num_rows == 1) {

            if (array_key_exists('mdOrder', $get)) {

                $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $get['mdOrder']);

                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Alfabank Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);

                if ($get['operation'] == 'approved') {
                    $status = 1;
                } elseif (($get['operation'] == 'deposited') and ((int)$get['status'] == 1)) {
                    $status = 2;
                } elseif ($get['operation'] == 'refunded') {
                    $status = 3;
                } elseif (($get['operation'] == 'reversed') or ($get['operation'] == 'declinedByTimeout')) {
                    $status = 4;
                } else {

                    $return['message'] = 'Тип события не поддерживается, запрос отклонен';
                    return $return;
                }

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $get['mdOrder']));
                //$redirect = $this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));

                return array(
                    'status' => $status,
                    'updated' => date('Y-m-d H:i:s')
                ); 
                
            }                    

        }
 
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения не действительны';
            return $return;
        }


        $response = array();
        $response['userName'] = $data['tokens']['userName'];
        $response['password'] = $data['tokens']['password'];
        
        $response['orderNumber'] = $data['uid'];
        $response['amount'] = floatval($data['payment']['amount']) * 100;
        $response['description'] = $data['payment']['description'];

        $response['returnUrl'] = $data['return_url'];
        $response['dynamicCallbackUrl'] = "https://payment.ecomkassa.ru/api/v1/webhooks/alfabank";
        
        
        if (mb_substr($response['userName'], 0, 2) == "r-")
            $this->base_url = self::API_BASE_URL_NEW;
        else
            $this->base_url = self::API_BASE_URL;


        $url = $this->base_url . '/register.do';

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($response)
            ),
            'ssl' => array(
                "verify_peer" => false,
                "verify_peer_name" => false
            )
        );

        $context  = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);


        if ($result === FALSE) {
            $return['error'] = true;
            $return['message'] = 'Сервер интернет-эквайринга Альфабанк не доступен';
            return $return;
        }

        $output = json_decode($result, true);

        if (array_key_exists('errorCode', $output)) {
            $return['error'] = true;
            if (array_key_exists('errorMessage', $output)) {
                $return['message'] = $output['errorMessage'];
            }
            return $return;
        }

        $sql_vars = array(
            $output['orderId'],
            $data['uid'],
            'alfabank',
            json_encode($data),
            json_encode($output, JSON_UNESCAPED_UNICODE),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);
        
        if ($insert->affectedRows() == 1) {
            return array(
                'payment_url' => $output['formUrl']
            );
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }

    public function payment_check($paymentId, $data_raw) {

        $return = array();

        $select = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $paymentId);
        $order = $select->fetchArray();

        if (mb_substr($data_raw['tokens']['userName'], 0, 2) == "r-")
            $this->base_url = self::API_BASE_URL_NEW;
        else
            $this->base_url = self::API_BASE_URL;

        $url = $this->base_url . '/getOrderStatusExtended.do';

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query(
                    array(
                        'userName' => $data_raw['tokens']['userName'],
                        'password' => $data_raw['tokens']['password'],
                        'orderId' => $paymentId
                    )
                )
            ),
            'ssl' => array(
                "verify_peer" => false,
                "verify_peer_name" => false
            )
        );

        

        $context  = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);

        

        if ($result === FALSE) {
            
            return array(
                'status' => $order['status'],
                'updated' =>$order['updated']
            );

        }

        $result = json_decode($result, true);

        if ( (array_key_exists('errorCode', $result)) and ($result['errorCode'] != '0') ) {
            $return['error'] = true;
            $return['message'] = 'Терминал вернул ошибку: '.$result['errorMessage'];
            return $return;
        }   


        if ($result['errorCode'] == '0'){
            
            if ($result['paymentAmountInfo']['paymentState'] == 'CREATED') {
                $status = 0;
            } elseif ($result['paymentAmountInfo']['paymentState'] == 'APPROVED') {
                $status = 1;
            } elseif ($result['paymentAmountInfo']['paymentState'] == 'DEPOSITED') {
                $status = 2;
            } elseif ( ($result['paymentAmountInfo']['paymentState'] == 'DECLINED') or ($result['orderStatus'] == 'REVERSED') or ($result['orderStatus'] == 'REFUNDED') ) {
                $status = 3;
            }

            if (isset($status)) {

                $data = json_decode($order['data_raw'],true);

                //$redirect = $this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => $order['updated']));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => $order['updated']));

                $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $paymentId));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $return['error'] = true;
                    return $return;
                }
            } else {
                $return['error'] = true;
                return $return;
            }

        } else {
            $return['error'] = true;
            return $return;
        }

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют учтеные данные для подключения к терминалу интернет-эквайринга Альфабанк';
            return $return;
        }

        if ( (!array_key_exists('userName', $data['tokens'])) and (!array_key_exists('password', $data['tokens'])) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Альфабанк';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    

}


