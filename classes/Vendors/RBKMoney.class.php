<?php

namespace Vendors;

/* Интернет-эквайринг RBKMoney */

class RBKMoney
{
	
	/* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
        $this->token = false;
    }


    public function webhook_insert($headers, $body)
    {
    
        if ( (array_key_exists('eventType', $body)) and (array_key_exists('invoice', $body)) ) {

            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($body), $body['invoice']['id']));

            /* Добавляем запись в БД */

            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('rbkmoney',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($body)) );
            $num_rows = $insert->affectedRows();

        
        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос RBKMoney отклонен';
            return $return;
        }
        
        if ($num_rows == 1) {

            

            if (array_key_exists('eventType', $body)) {

                $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $body['invoice']['id']);
  
                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (RBKMoney Webhooks)';
                    return $return;
                }

                

                $data = json_decode($order['data_raw'],true);


                if ($body['eventType'] == 'InvoiceCreated') {
                    $status = 1;
                } elseif ( ($body['eventType'] == 'InvoicePaid') or ($body['Status'] == 'InvoiceFulfilled') ) {
                    $status = 2;
                } elseif ($body['eventType'] == 'InvoiceCancelled') {
                    $status = 3;
                } else {

                    $return['message'] = 'Тип события RBKMoney не поддерживается, запрос отклонен';
                    return $return;
                }

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $body['invoice']['id']));
                //$this->redirect($data['callback_url'], array('uid' => $body['invoice']['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $body['invoice']['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                
            }                    

        }
        
 
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $response = array();


        $response['shopID'] = $data['tokens']['shopID'];
        $response['externalID'] = $this->genUID();
        $response['dueDate'] = date("Y-m-d\TH:i:s\Z", strtotime("6 hour"));
        $response['amount'] = (int) $data['payment']['amount'] * 100;
        $response['product'] = $data['payment']['description'];
        $response['currency'] = 'RUB';
        $response['metadata'] = (object) array();

        $output = $this->api("https://api.rbk.money/v2/processing/invoices", $response);
        
        if (array_key_exists('invoiceAccessToken', $output)) {

            $shortUrl = $this->api(
                "https://rbk.mn/v1/shortened-urls",
                array(
                    'sourceUrl' => 'https://checkout.rbk.money/v1/checkout.html?name=&description=&email=&redirectUrl='.urlencode($data['return_url']).'&terminals=false&wallets=false&bankCard=true&applePay=false&googlePay=false&samsungPay=false&invoiceID='.$output['invoice']['id'].'&invoiceAccessToken='.$output['invoiceAccessToken']['payload'],
                    'expiresAt' => $response['dueDate']
                )
            );

            if ((is_array($shortUrl)) and (array_key_exists('shortenedUrl', $shortUrl))) {

                $sql_vars = array(
                    $output['invoice']['id'],
                    $data['uid'],
                    'rbkmoney',
                    json_encode($data),
                    json_encode($output),
                    0
                );
        
                $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);
                if ($insert->affectedRows() == 1) {
                    
                    return array(
                        'payment_url' => $shortUrl['shortenedUrl']
                    );
                }
                else  {
                    $return['error'] = true;
                    $return['message'] = 'Произошла ошибка при записи данных в БД';
                    return $return;
                } 
            } else {
                $return['error'] = true;
                $return['message'] = 'Произошла ошибка при формировании короткой ссылки';
                return $return;
            }
    
    
            return $response;

        } else {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при формировании запроса: '.json_encode($output);
            return $return;
        }


    }

    public function payment_check($paymentId, $data_raw) {
        
        $this->token = $data_raw['tokens']['token'];
        
        $response = $this->api("https://api.rbk.money/v2/processing/invoices/".$paymentId, $data);

        if ((is_array($response)) and (array_key_exists('status', $response))) {
            if (($response['status'] == 'paid') or ($response['status'] == 'fulfilled')) {
                $status = 2;
            } elseif ($response['status'] == 'cancelled')  {
                $status = 3;
            } elseif ($response['status'] == 'unpaid')  {
                $status = 1;
            } else {
                $status = 0;
            }

            $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['id']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $select = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $response['id']);
                    $order = $select->fetchArray();

                    return array(
                        'status' => $order['status'],
                        'updated' => $order['updated']
                    );
                }

        } else {
            $return['error'] = true;
            $return['message'] = 'Неизвестная ошибка';
            return $return;
        }

        $return = array();

        return $response;

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует API-ключ для подключения к терминалу интернет-эквайринга RBKMoney';
            return $return;
        }

        if (!array_key_exists('token', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует API-ключ для подключения к терминалу интернет-эквайринга RBKMoney';
            return $return;
        }

        if (!array_key_exists('shopID', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Не указан идентификатор магазина shopID';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    
    private function api($method,$data = false, $httpcode = false){
                                                                                                                                                                                                           
        $ch = curl_init($method);

        if ($data) {
            $data_string = json_encode($data);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            if (array_key_exists('externalID',$data))
                $externalID = $data['externalID'];
            else $externalID = $this->genUID();
        } else {
            $externalID = $this->genUID();
        }                                                                   
                                                                      
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json; charset=utf-8',
            'X-Request-ID: '.$externalID,
            'Authorization: Bearer '. $this->token                                                     
        ));

        $result = curl_exec($ch);

        if ($httpcode) {
            return array(
                'httpcode' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
                'body' => $result
            );
        }

        return json_decode($result, true);
    }

    public function check($data){

        if (!array_key_exists('token', $data)) {
            $return['error'] = true;
            $return['message'] = 'Не указан API-ключ для подключения к терминалу интернет-эквайринга RBKMoney';
            return $return;
        }

        $this->token = $data['token'];

        $response = $this->api("https://api.rbk.money/v2/processing/invoices/1", false, true);

        $return = array();

        if ($response['httpcode'] == 401){
            $return['error'] = true;
            $return['message'] = 'API-ключ для подключения к терминалу интернет-эквайринга RBKMoney не действителен';
            return $return;
        } elseif ($response['httpcode'] == 404) {
            $return['message'] = 'API-ключ для подключения к терминалу интернет-эквайринга RBKMoney действителен';
            return $return;
        } else {
            $return['message'] = 'Неизвестная ошибка';
            return $return;
        }
     
    }

    private function genUID() {
        $uid = uniqid(NULL, TRUE);
        $rawid = strtoupper(sha1(uniqid(rand(), true)));
        $result = substr($uid, 6, 8);
        $result .= substr($uid, 0, 4);
        $result .= substr(sha1(substr($uid, 3, 3)), 0, 4);
        $result .= substr(sha1(substr(time(), 3, 4)), 0, 4);
        $result .= strtolower(substr($rawid, 10, 12));
        return $result;
    }

}


