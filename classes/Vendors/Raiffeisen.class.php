<?php

namespace Vendors;

/* Интернет-эквайринг Raiffeisen */

class Raiffeisen
{


	/* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
        $this->login = false;
        $this->pass = false;
        $this->token = null;

        $this->test = false;
        
    }

    public function webhook_insert($headers, $body)
    {
        
        if ( (array_key_exists('qrId', $body)) and (array_key_exists('order', $body)) ) {

            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($body, JSON_UNESCAPED_UNICODE), (string)$body['qrId']));

            /* Добавляем запись в БД */
            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('raiffeisen',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($body, JSON_UNESCAPED_UNICODE)) );
            $num_rows = $insert->affectedRows();

        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Raiffeisen Webhooks отклонен';
            return $return;
        }
        
        if ($num_rows == 1) {

            if (array_key_exists('paymentStatus', $body)) {

                $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $body['qrId']);

                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Raiffeisen Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);


                if ($body['paymentStatus'] == 'SUCCESS') {
                    $status = 2;
                } else {

                    $return['message'] = 'Тип события Raiffeisen не поддерживается, запрос отклонен';
                    return $return;
                }

                $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, (string)$body['qrId']));
                //$this->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                
            }                    

        }
 
    }

    public function check($data){


        if (!isset($data['SecretKey']))
            return [
                "error" => true,
                "message" => "Не указан SecretKey для подключения к терминалу интернет-эквайринга Райффайзен"
            ];

        if (isset($data['test']))
            $this->test = true;


        $this->token = $data['SecretKey'];

        $webhook = $this->createWebhook();

        if ($webhook == null) {
            return [
                "error" => true,
                "message" => "SecretKey для подключения к терминалу интернет-эквайринга Райффайзен не действителен"
            ];
        }

        return [
            'message' => 'SecretKey для подключения к терминалу интернет-эквайринга Райффайзен действителен'
        ];

     
    }

    public function createWebhook() {
        return $this->post("/settings/v1/callback", ["callbackUrl" => "https://env-5581354.mircloud.ru/api/v1/webhooks/raiffeisen"]);
    }

    private function post($method,$data){
        $this->api_url = $this->test ? "https://pay-test.raif.ru/api" : "https://pay.raif.ru/api";
        $data_string = json_encode($data);                                                                                                                                                                                                       
        $ch = curl_init($this->api_url . $method);                                                             
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json; charset=utf-8',                                                                                
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer '. $this->token                                                                   
        ));

        $result = curl_exec($ch);
        return json_decode($result, true);
    }


    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $response = [
            "amount" => floatval($data['payment']['amount']),
            "currency" => "RUB",
            "order" => $data['uid'],
            "paymentDetails" =>  $data['payment']['description'],
            "qrType" => "QRDynamic",
            "qrExpirationDate" => date("c", time() + 60),
            "sbpMerchantId" => $data['tokens']['sbpMerchantId'],
            "redirectUrl" => $data['return_url']
        ];

        $output = $this->post("/sbp/v2/qrs", $response);
        
        if ((isset($output['code']))) {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при создании платежа';
            $return['data'] = $output;
            return $return;
        }

        $sql_vars = array(
            $output['qrId'],
            $data['uid'],
            'raiffeisen',
            json_encode($data),
            json_encode($output),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);
        if ($insert->affectedRows() == 1) {
            return array(
                'payment_url' => $output['payload']
            );
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }

    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Райффайзен';
            return $return;
        }

        if (!array_key_exists('SecretKey', $data['tokens']) || !array_key_exists('sbpMerchantId', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют SecretKey/sbpMerchantId для подключения к терминалу интернет-эквайринга Райффайзен';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }


}
