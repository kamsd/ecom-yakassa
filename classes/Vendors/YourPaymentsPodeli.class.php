<?php

namespace Vendors;

class YourPaymentsPodeli
{
	
    const YOURPAYMENTS_DOMAIN = "https://secure.ypmn.ru";
    const YOURPAYMENTS_DOMAIN_TEST = "https://sandbox.ypmn.ru";
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
        $this->merchantCode = "";
        $this->secret = "";
        $this->test = false;

        $this->vendorCode = "yourpayments_podeli";
    }


    public function webhook_insert($headers, $body)
    {

        if (!array_key_exists('orderData', $body))
            return [
                "error" => true,
                "message" => "Запрос YourPayments Webhooks отклонен"
            ];

        $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', [
            $this->vendorCode,
            json_encode($headers, JSON_UNESCAPED_UNICODE),
            json_encode($body, JSON_UNESCAPED_UNICODE)
        ]);

        $select = $this->db->query('SELECT * FROM orders WHERE uid = ? and external_uid = ?', [$body['orderData']['payuPaymentReference'], $body['orderData']['merchantPaymentReference']]);

        if ($select->numRows() != 0) {
            $order = $select->fetchArray();
            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ? and external_uid = ?', array(json_encode($body, JSON_UNESCAPED_UNICODE), $body['orderData']['payuPaymentReference'], $body['orderData']['merchantPaymentReference']));
        } else
            return [
                "error" => true,
                "message" => "Транзакция не найдена в БД (YourPayments Webhooks)"
            ];

        $data = json_decode($order['data_raw'], true);

        $orderDataStatus = $body['orderData']['status'];

        if ($orderDataStatus == 'PAYMENT_AUTHORIZED')
            $status = 1;
        elseif ($orderDataStatus == 'COMPLETE')
            $status = 2;
        elseif ($orderDataStatus == 'REVERSED')
            $status = 3;
        else 
            return [
                "error" => true,
                "message" => "Тип события YourPayments не поддерживается, запрос отклонен"
            ];

        $this->db->query('UPDATE orders SET status = ? WHERE uid = ? and external_uid = ?', [$status, $body['orderData']['payuPaymentReference'], $body['orderData']['merchantPaymentReference']]);

        if (isset($data['callback_url']))
            (new \Redirect())->redirect($data['callback_url'], ['uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')]);

        return [
            "success" => true
        ];
 
    }

    public function payment_new($data)
    {
        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check))
            return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданным UID уже существует';
            return $return;
        }

        $this->test = (array_key_exists("test", $data['tokens']) && $data['tokens']["test"] == 1);

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals))
            return [
                "error" => true,
                "message" => "Учетные данные для подключения заданы неверно"
            ];


        $output = $this->api("/api/v4/payments/authorize", [
            "merchantPaymentReference" => $data['uid'],
            "currency" => "RUB",
            "returnUrl" => $data['return_url'],
            "authorization" => [
                "paymentMethod" => "BNPL",
                "usePaymentPage" => "NO"
            ],
            "client" => [
                "billing" => [
                    "firstName" => "-",
                    "lastName" => "-",
                    "email" => "no-reply@ecomkassa.ru",
                    "countryCode" => "RU",
                    "phone" => "+79999999999"
                ]
            ],
            "products" => [
                [
                    "name" => $data['payment']['description'],
                    "sku" => $data['uid'],
                    "unitPrice" => round(floatval($data['payment']['amount']), 2),
                    "quantity" => "1",
                    "marketplace" => [
                        "version" => 1,
                        "merchantCode" => $this->merchantCode
                    ]
                ]
            ]
        ]);

        if (array_key_exists("status", $output) && $output["status"] == "SUCCESS" && array_key_exists("paymentResult", $output)) {
            //$output["PaymentURL"] = $output["paymentResult"]["url"];
        } else if (array_key_exists("status", $output) && $output["status"] != "SUCCESS" && array_key_exists("message", $output))
            return [
                "error" => true,
                "message" => $output["message"]
            ];
        else
            return [
                "error" => true,
                "message" => "Ошибка шлюза платежной системы, попробуйте позже."
            ];

        $sql_vars = array(
            $output['payuPaymentReference'],
            $data['uid'],
            $this->vendorCode,
            json_encode($data),
            json_encode($output),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);

        if ($insert->affectedRows() == 1) {

            return array(
                'payment_url' => $output["paymentResult"]["url"]
            );

        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }

    public function payment_check($paymentId, $data_raw) {
        
        $this->merchantCode = $data_raw['tokens']['merchantCode'];
        $this->secret = $data_raw['tokens']['secret'];

        $this->test = (array_key_exists("test", $data_raw['tokens']) && $data_raw['tokens']["test"] == 1);

        $response = $this->api("/api/v4/payments/status/" . $paymentId);
 
        $return = array();

        if ($response['code'] == '200') {
            
            if ($response['paymentStatus'] == 'PAYMENT_AUTHORIZED')
                $status = 1;
            elseif ($response['paymentStatus'] == 'COMPLETE')
                $status = 2;
            elseif ($response['paymentStatus'] == 'REVERSED')
                $status = 3;

            if (isset($status)) {
                $update = $this->db->query('UPDATE orders SET status = ? WHERE external_uid = ?', array($status, $paymentId));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $return['error'] = true;
                    return $return;
                }
            } else {
                $return['error'] = true;
                return $return;
            }

        } else {
            return [
                "error" => true,
                "message" => "Не удалось получить данные о платеже"
            ];
        }

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу YourPayments';
            return $return;
        }

        if ( (!array_key_exists('merchantCode', $data['tokens'])) || (!array_key_exists('secret', $data['tokens'])) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу YourPayments';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        return array();

    }

    
    private function api($url, $data = false) {

        $ctime = date("Y-m-d\TH:i:s");

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ($this->test ? self::YOURPAYMENTS_DOMAIN_TEST : self::YOURPAYMENTS_DOMAIN) . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $data ? 'POST' : 'GET',
            CURLOPT_POSTFIELDS => json_encode($data, JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => array(
              'Accept: application/json',
              'X-Header-Signature: '.$this->getSignature($url, $ctime, $data ? 'POST' : 'GET', json_encode($data, JSON_UNESCAPED_UNICODE)),
              'X-Header-Merchant: '.$this->merchantCode,
              'X-Header-Date: '.$ctime . "+03:00",
              'Content-Type: application/json'
            ),
          ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);

    }

    private function getSignature($method, $ctime, $requestType, $data = false){
        $string = $this->merchantCode . $ctime . "+03:00" . $requestType  . $method;

        if ($data) {
            $string .= md5($data);
        }
        return hash_hmac('sha256', $string, $this->secret);
    }


    public function check($data){

        if (!array_key_exists('merchantCode', $data) || !array_key_exists('secret', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи для подключения к терминалу YourPayments';
            return $return;
        }

        $this->merchantCode = $data['merchantCode'];
        $this->secret = $data['secret'];

        $response = $this->api("/api/v4/payments/status/0");

        if (array_key_exists('code', $response) && $response['code'] == 401) {
            return [
                "error" => true,
                "message" => "Ключ доступа к терминалу YourPayments не действителен"
            ];
        } else {
            return [
                "message" => "Ключ доступа к терминалу YourPayments действителен"
            ];
        }
     
    }

}


