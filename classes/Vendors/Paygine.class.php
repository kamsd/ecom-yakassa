<?php

namespace Vendors;

/* Интернет-эквайринг Paygine */

class Paygine
{

    const PAYMENT_TYPES = [
        "podeli", "plate", "card", "sbp"
    ];

    const API_URL_TEST = "https://test.paygine.com/webapi";
    const API_URL_PROD = "https://pay.paygine.com/webapi";
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
        $this->sectorId = false;
        $this->password = false;
        $this->test = false;
        $this->source = "paygine";
    }

    public function webhook_insert($body) {

        $array = $this->xmlToArray($body);
    
        if ($array && (isset($array['order_id']))) {
            $response = $this->db->query(
                'UPDATE orders SET data_callback = ? WHERE uid = ? and source = ?',
                [
                    json_encode($array, JSON_UNESCAPED_UNICODE),
                    $array['order_id'],
                    $this->source
                ]
            );
            $insert = $this->db->query(
                'INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)',
                [
                    $this->source,
                    [],
                    json_encode($array, JSON_UNESCAPED_UNICODE)
                ]
            );
        } else
            return [
                'error' => true,
                'Запрос не в формате XML'
            ];


        if ($insert->affectedRows() == 1) {

            if (array_key_exists('order_state', $array)) {

                $select = $this->db->query(
                    'SELECT * FROM orders WHERE uid = ? and source = ?',
                    [
                        $array['order_id'],
                        $this->source
                    ]
                );
  
                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);

                if (in_array($array['order_state'], ["AUTHORIZED"]))
                    $status = 1;
                elseif (in_array($array['order_state'], ["COMPLETED"]))
                    $status = 2;
                elseif (in_array($array['order_state'], ["CANCELED"]))
                    $status = 3;
                else
                    return [
                        'message' => 'Тип события не поддерживается, запрос отклонен'
                    ];

                $this->db->query(
                    'UPDATE orders SET status = ? WHERE uid = ? and source = ?',
                    [
                        $status,
                        $array['order_id'],
                        $this->source
                    ]
                );

                if (isset($data['callback_url']))
                    (new \Redirect())->redirect($data['callback_url'], ['uid' => $array['order_id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')]);
                
            }                    

        }
        
    }

    public function payment_new($data) {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        if (isset($data["tokens"]["test"]))
            $this->test = true;

            
        $order = $this->api(
            "/Register",
            [
                'amount' => round(floatval($data['payment']['amount']), 2) * 100,
                'currency' => 643,
                'description' => $data['payment']['description'],
                'reference' => $data['uid'],
                'url' => $data['return_url'],
                'failurl' => $data['return_url']
            ]
        );

        if (isset($order['error']))
            return $order;
        
        $method = "/custom/svkb/PurchaseWithInstallment";

        if (isset(($data["tokens"]["payment_type"]))) {
            switch ($data["tokens"]["payment_type"]) {
                case "podeli":
                    $method = "/custom/alfa/PodeliPWI";
                    break;
                case "plate":
                    $method = "/custom/svkb/PurchaseWithInstallment";
                    break;
                case "card":
                    $method = "/Purchase";
                    break;
                case "sbp":
                    $method = "/PurchaseSBP";
                    break;
            }
        }

        // 

      
        $output = $this->api(
            $method,
            [
                'id' => $order["id"]
            ],
            true
        );

        if ((isset($output['payment_url'])) and (!isset($output['error']))) {

            $insert = $this->db->query(
                'INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)',
                [
                    $order['id'],
                    $data['uid'],
                    'paygine',
                    json_encode($data, JSON_UNESCAPED_UNICODE),
                    json_encode(array_merge($order, $output), JSON_UNESCAPED_UNICODE),
                    0
                ]
            );

            if ($insert->affectedRows() == 1) {
                
                return array(
                    'payment_url' => $output['payment_url']
                );
            }
            else  {
                $return['error'] = true;
                $return['message'] = 'Произошла ошибка при записи данных в БД';
                return $return;
            } 
    
            return $response;

        } elseif (isset($output['error'])) {

            $return['error'] = true;
            $return['message']= $output['description'];
            return $return;

        } else {

            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при формировании короткой ссылки';
            return $return;

        }


    }

    public function payment_check($paymentId, $data_raw) {
        
        $this->token = $data_raw['tokens']['token'];
        
        $response = $this->api("/GetPaymentByOrder", $data);

        if ((is_array($response)) and (array_key_exists('status', $response))) {
            if (($response['status'] == 'paid') or ($response['status'] == 'fulfilled')) {
                $status = 2;
            } elseif ($response['status'] == 'cancelled')  {
                $status = 3;
            } elseif ($response['status'] == 'unpaid')  {
                $status = 1;
            } else {
                $status = 0;
            }

            $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['id']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $select = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $response['id']);
                    $order = $select->fetchArray();

                    return array(
                        'status' => $order['status'],
                        'updated' => $order['updated']
                    );
                }

        } else {
            $return['error'] = true;
            $return['message'] = 'Неизвестная ошибка';
            return $return;
        }

        $return = array();

        return $response;

    }
    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют учетные данные tokens для подключения к терминалу интернет-эквайринга';
            return $return;
        }

        if (!array_key_exists('sectorId', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует sectorId для подключения к терминалу интернет-эквайринга';
            return $return;
        }


        if (array_key_exists('payment_type', $data['tokens']) && !in_array($data['tokens']['payment_type'], self::PAYMENT_TYPES)) {
            $return['error'] = true;
            $return['message'] = 'Недопустимое значение payment_type';
            return $return;
        }

        if (!array_key_exists('password', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует password (пароль для подписи) для подключения к терминалу интернет-эквайринга';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }

        return [];

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data, JSON_UNESCAPED_UNICODE);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    
    private function api($method, $params = [], $returnUrl = false) {
                  
        $params["sector"] = $this->sectorId;
        $params["signature"] = $this->getSign($params);

        $curl = curl_init();

        $url = ($this->test ? self::API_URL_TEST : self::API_URL_PROD) . $method . '?' . http_build_query($params);

        if ($returnUrl)
            return [
                "payment_url" => $url
            ];

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        $array = $this->xmlToArray($response);

        if (isset($array["code"]))
            return [
                "error" => true,
                "message" => $array["description"] ?? "Неизвестная ошибка",
                "code" => $array["code"]
            ];

        return $array;

    }

    private function getSign($params) {
        $strtosign = ($params["sector"] ?? "") . ($params["id"] ?? "") . ($params["amount"] ?? "") . ($params["currency"] ?? "") . ($this->password ?? "");
        return base64_encode(hash('sha256', $strtosign));
    }

    public function check($data) {

        if (!array_key_exists('sectorId', $data)) {
            $return['error'] = true;
            $return['message'] = 'Не указан sectorId для подключения к терминалу интернет-эквайринга';
            return $return;
        }

        if (!array_key_exists('password', $data)) {
            $return['error'] = true;
            $return['message'] = 'Не указан password (пароль для подписи) для подключения к терминалу интернет-эквайринга';
            return $return;
        }

        $this->sectorId = $data['sectorId'];
        $this->password = $data['password'];

        if (isset($data["test"]))
            $this->test = true;

        $response = $this->api(
            "/Order",
            [
                "id" => 0
            ]
        );

        $return = [];
        
        if ( (is_array($response)) and (isset($response['code'])) ) {
            
            if ((int)$response['code'] == 101){
                $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга действительны';
            } else {
                $return['error'] = true;
                $return['message'] = 'Учетные данные для подключения к терминалу интернет-эквайринга не действительны';
            }

        } else {
            $return['error'] = true;
            $return['message'] = 'Шлюз интернет-эквайринга не доступен, попробуйте позже'; 
        }

        return $return; 
     
    }

    private function xmlToArray($string) {
        $xml = @simplexml_load_string($string);
        if (!$xml)
            return false;
        $json = json_encode($xml, JSON_UNESCAPED_UNICODE);
        return json_decode($json, true);
    }

}


