<?php

namespace Vendors;

/* Интернет-эквайринг Robokassa */

class Robokassa
{
	
	/* Constructor */
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB(); // подключение к БД
        $this->login = false;
        $this->apikey = false;
    }


    public function webhook_insert($headers, $body)
    {

        if ((is_array($body)) and (isset($body['InvId']))) {

            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', array(json_encode($body), $body['InvId']));

            /* Добавляем запись в БД */

            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', array('robokassa',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($body)) );
            $num_rows = $insert->affectedRows();

        
        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Robokassa отклонен';
            return $return;
        }

        if ($num_rows == 1) {

            $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $body['id']);

            if ($select->numRows() != 0) {
                $order = $select->fetchArray();
            } else {
                $return['error'] = true;
                $return['message'] = 'Транзакция не найдена в БД (Robokassa Webhooks)';
                return $return;
            }

            $data = json_decode($order['data_raw'],true);

            $status = 2;

            $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $body['id']));
            //$this->redirect($data['callback_url'], array('uid' => $body['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));   
            (new \Redirect())->redirect($data['callback_url'], array('uid' => $body['id'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));               

        }
        
        
 
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }

        $crc = md5($data["tokens"]["login"].":".$data["payment"]["amount"].":".$data['uid'].":".$data["tokens"]["pass1"]);

        $payment_url = "https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=".$data["tokens"]["login"]."&OutSum=".$data["payment"]["amount"]."&InvoiceID=".$data['uid']."&Description=".$data["payment"]["description"]."&SignatureValue=".$crc; 

        $output = array(
            'payment_url' => $payment_url
        );

        
        $sql_vars = array(
            $data['uid'],
            $data['uid'],
            'robokassa',
            json_encode($data),
            json_encode($output),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);

        if ($insert->affectedRows() == 1) {
            
            return array(
                'payment_url' => $output['payment_url']
            );
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 
            

    }

    /*
    public function payment_check($paymentId, $data_raw) {

        // МЕТОД
        // https://docs.robokassa.ru/#2338
        
        $this->token = $data_raw['tokens']['token'];
        
        $response = $this->api("/GetPaymentByOrder", $data);

        if ((is_array($response)) and (array_key_exists('status', $response))) {
            if (($response['status'] == 'paid') or ($response['status'] == 'fulfilled')) {
                $status = 2;
            } elseif ($response['status'] == 'cancelled')  {
                $status = 3;
            } elseif ($response['status'] == 'unpaid')  {
                $status = 1;
            } else {
                $status = 0;
            }

            $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['id']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $select = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $response['id']);
                    $order = $select->fetchArray();

                    return array(
                        'status' => $order['status'],
                        'updated' => $order['updated']
                    );
                }

        } else {
            $return['error'] = true;
            $return['message'] = 'Неизвестная ошибка';
            return $return;
        }

        $return = array();

        return $response;

    }
    */

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют учетные данные tokens для подключения к терминалу интернет-эквайринга Robokassa';
            return $return;
        }

        if (!array_key_exists('login', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует login для подключения к терминалу интернет-эквайринга Robokassa';
            return $return;
        }

        if (!array_key_exists('pass1', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует пароль #1 для подключения к терминалу интернет-эквайринга Robokassa';
            return $return;
        }

        if (!array_key_exists('pass2', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует пароль #2 для подключения к терминалу интернет-эквайринга Robokassa';
            return $return;
        }

        return array();

    }

}


