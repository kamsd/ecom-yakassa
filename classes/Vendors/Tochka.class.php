<?php

namespace Vendors;

/* Интернет-эквайринг Точка Банк */

class Tochka
{

    const BIK_CODES = ["044525999", "044525104"];
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();

        $this->client_id = TOCHKA_CLIENT_ID;
        $this->client_secret = TOCHKA_CLIENT_SECRET;
        $this->access_token = null;
        $this->consentId = null;

        $this->as_url = "https://enter.tochka.com";
        $this->rs_url = "https://enter.tochka.com/uapi";
        $this->api_version = "v1.0";

        $this->accesstoken_hybrid = null;
        $this->customerCode = null;
        $this->legalId = null;
        $this->merchantId = null;
        $this->accountId = null;
        $this->bankCode = null;

        $this->redirect_uri = TOCHKA_DOMAIN."/api/v1/tochka/change_token";

    }


    public function webhook_insert($headers, $jwtToken)
    {
    
        $jwtArr = array_combine(['header', 'payload', 'signature'], explode('.', $jwtToken));
        $webhook = json_decode(base64_decode($jwtArr['payload']), true);

        $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', ['tochka',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($jwtToken, JSON_UNESCAPED_UNICODE)] );

        $uid = null;
        

        if (isset($webhook['purpose'])) {
            foreach (explode(" ", $webhook['purpose']) as $part) { 
                if (mb_strlen($part) >= 32) {
                    $uid = $part;
                    break;
                }
            }

            

            if (isset($webhook['webhookType']) && $webhook['webhookType'] == "acquiringInternetPayment") {
                if (isset($webhook['operationId'])) {
                    $uid = $webhook['operationId'];
                } else {

                    $sqlVars = [
                        2,
                        date("Y-m-d H:i:s", strtotime('-24 hours', strtotime('now'))),
                        "%".$webhook["amount"]."%"
                    ];

                    $sql = "SELECT * FROM orders WHERE status != ? and created > ? and data_raw like ? ORDER BY id DESC limit 1";

                    $orderData = $this->db->query($sql, $sqlVars)->fetchArray();

                    if (count($orderData) > 0)
                        $uid = $orderData["uid"];
                    else
                        $uid = NULL;

                }
            }
                

            if (is_null($uid)) return;
            
            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE uid = ?', [$jwtToken, $uid]);

            $select = $this->db->query('SELECT * FROM orders WHERE uid = ?', $uid);
            
            if ($select->numRows() != 0) {
                $order = $select->fetchArray();
                $data = json_decode($order['data_raw'],true);

                $_GET['client_token'] = $data['client_token'];
                $this->getNewToken();
                $callback = $this->payment_check($uid, $data, $webhook['webhookType']);

                if (isset($data['callback_url'])) {
                    //$this->redirect($data['callback_url'], array('uid' => $uid, 'status' => $callback['status'], 'time' => date('Y-m-d H:i:s')));
                    (new \Redirect())->redirect($data['callback_url'], array('uid' => $uid, 'status' => $callback['status'], 'time' => date('Y-m-d H:i:s')));
                }

            } else {
                return [
                    'error' => true,
                    'message' => 'Транзакция не найдена в БД (Tochka Webhooks)'
                ];
            }

            /*
            $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', [2, $uid]);
            */

        }

    }


    public function getLink()
    {
        $dataToken = $this->getToken();
        if (!isset($dataToken['access_token']))
            return $this->error("Не удалось получить токен приложения", $dataToken);
        else $this->access_token = $dataToken['access_token'];

        $dataPermissions = $this->createPermissions();

        if (!isset($dataPermissions['Data']) || !isset($dataPermissions['Data']['consentId']))
            return $this->error("Не удалось получить права для приложения", $dataPermissions);
        else {
            $this->consentId = $dataPermissions['Data']['consentId'];
        }

        $bankCode = (isset($_GET["bankCode"]) && in_array($_GET["bankCode"], self::BIK_CODES)) ? $_GET["bankCode"] : null;

        $state = $this->genUID($bankCode);

        return [
            "link" => $this->as_url.'/connect/authorize?client_id='.$this->client_id.'&response_type=code id_token&state='.$state.'&redirect_uri='.$this->redirect_uri.'&scope=accounts&consent_id='.$this->consentId,
            "client_token" => $state
        ];
        
    }

    public function getToken() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->as_url.'/connect/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&grant_type=client_credentials&scope=accounts%20cards%20customers%20sbp%20payments',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            )
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);

    }

    public function createPermissions() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->rs_url.'/'.$this->api_version.'/consents',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
            "Data": {
                "permissions": [
                "ReadAccountsBasic",
                "ReadAccountsDetail",
                "ReadBalances",
                "ReadStatements",
                "ReadCustomerData",
                "ReadSBPData",
                "EditSBPData",
                "CreatePaymentForSign",
                "CreatePaymentOrder",
                "ManageWebhookData",
                "MakeAcquiringOperation",
                "ReadAcquiringData"
                ],
                "expirationDateTime": "2030-10-03T00:00:00+00:00"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->access_token,
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);

    }

    public function changeCodeToToken() {

        if (!isset($_GET['code']))
            return $this->error("Не найден code", $_GET);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->as_url.'/connect/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&grant_type=authorization_code&scope=accounts%20cards%20customers%20sbp%20payments&code='.$_GET['code'].'&redirect_uri='.$this->redirect_uri,
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
        ));
          
        $response = curl_exec($curl);
        
        curl_close($curl);
        $data = json_decode($response, true);

        if (is_null($data))
            return $this->error("Ссылка не действительна", $_GET);

        $select = $this->db->query('SELECT uid FROM tochka_tokens WHERE uid = ?', $_GET['state']);

        if ($select->numRows() != 0)
            return [
                "client_token" => $_GET['state']
            ];

        $this->accesstoken_hybrid = $data["access_token"];

        $this->getCustomer();

        if (strpos($_GET['state'], '-') !== false) {
            $this->bankCode = explode("-", $_GET['state'])[1];
        }

        $customer = $this->getCustomerInfo();

        if (!isset($customer['Data'])) {
            return $customer;
        }

        $this->createWebhook();

        $this->db->query('INSERT INTO tochka_tokens (uid, access_token, refresh_token, customerCode, legalId, merchantId, accountId) VALUES (?,?,?,?,?,?,?)',
            [
                $_GET['state'],
                $data['access_token'],
                $data['refresh_token'],
                $this->customerCode,
                $this->legalId,
                $this->merchantId,
                $this->accountId
            ]);

        return [
            "client_token" => $_GET['state']
        ];

    }


    public function getNewToken() {

        if (!isset($_GET['client_token']))
            return $this->error("Параметр client_token не задан");
  
        $select = $this->db->query('SELECT * FROM tochka_tokens WHERE uid = ? LIMIT 1', $_GET['client_token']);

        if ($select->numRows() == 0)
            return $this->error("Пользователь не найден");
        
        $tokens = $select->fetchArray();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->as_url.'/connect/token',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&grant_type=refresh_token&refresh_token='.$tokens['refresh_token'],
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        $data = json_decode($response, true);

        if (!is_array($data) || !isset($data['access_token']))
            return $this->error("Не удалось получить access_token");

        $this->db->query('UPDATE tochka_tokens SET access_token = ?, refresh_token = ? WHERE uid = ? LIMIT 1', [$data['access_token'], $data['refresh_token'], $_GET['client_token']]);

        $this->accesstoken_hybrid = $data['access_token'];
        $this->customerCode = $tokens['customerCode'];
        $this->legalId = $tokens['legalId'];
        $this->merchantId = $tokens['merchantId'];
        $this->accountId = $tokens['accountId'];

        if (strpos($tokens['uid'], '-') !== false) {
            $this->bankCode = explode("-", $tokens['uid'])[1];
        }
        
        return $data;

    }

    public function payment_new($data) {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданныйм UID уже существует';
            return $return;
        }
        
        $_GET['client_token'] = $data['tokens']['client_token'];
        $token = $this->getNewToken();

        if (isset($token["error"]))
            return $token;

        $curl = curl_init();


        $isSBP = true;

        if (isset($data['tokens']['payment_type']) && $data['tokens']['payment_type'] == "acquiring")
            $isSBP = false;

    

        if ($isSBP) {

            $payment = [
                "client_token" => $data['tokens']['client_token'],
                "callback_url" => $data['callback_url'],
                "Data" => [
                    "merchantId" => $this->merchantId,
                    "legalId" => $this->legalId,
                    "customerCode" => $this->customerCode,
                    "amount" => round(floatval($data['payment']['amount']) * 100),
                    "currency" => "RUB",
                    "paymentPurpose" => $data['payment']['description'],
                    "qrcType" => "02",
                    "imageParams" => [
                        "width" => 200,
                        "height" => 200,
                        "mediaType" => "image/png"
                    ],
                    "sourceName" => "string",
                    
                ]
            ];
    
            $url = $this->rs_url.'/sbp/'.$this->api_version.'/qr-code/merchant/'.$this->merchantId.'/'.$this->accountId;

        } else {

            $payment = [
                "client_token" => $data['tokens']['client_token'],
                "callback_url" => $data['callback_url'],
                "Data" => [
                    "customerCode" => $this->customerCode,
                    "amount" => $data['payment']['amount'],
                    "purpose" => $data['payment']['description'],
                    "redirectUrl" => $data['return_url'],
                    "paymentMode" => ["card", "sbp"]
                ]
            ];
    
            $url = $this->rs_url.'/acquiring/'.$this->api_version.'/payments';
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($payment, JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->accesstoken_hybrid,
                'Content-Type: application/json'
            )
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $output = json_decode($response, true);

        if (!isset($output['Data'])) {
            return [
                "error" => true,
                "message" => "Не удалось создать ссылку",
                "payload" => $output
            ];
        }

        $sql_vars = array(
            $isSBP ? $output['Data']['qrcId'] : $output['Data']['operationId'],
            $data['uid'],
            'tochka',
            json_encode($payment, JSON_UNESCAPED_UNICODE),
            json_encode($output, JSON_UNESCAPED_UNICODE),
            0
        );


        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);
        if ($insert->affectedRows() == 1) {
            return array(
                'payment_url' => $isSBP ? str_replace("https://qr.nspk.ru/", "https://web.qr.nspk.ru/", $output['Data']['payload']) : $output['Data']['paymentLink']
            );
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data) || !is_array($data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data) || !is_array($data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Точка Банк';
            return $return;
        }

        if (!array_key_exists('client_token', $data['tokens'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Точка Банк';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        return array();

    }


    public function getCustomer() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->rs_url.'/open-banking/'.$this->api_version.'/customers',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->accesstoken_hybrid
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $customers = json_decode($response, true);

        foreach ($customers['Data']['Customer'] as $c) {
            if ($c['customerType'] == "Business") {
                $this->customerCode = $c['customerCode'];
                return $c['customerCode'];
            }
        }
        return null;
    }

    public function getCustomerInfo() {

        $curl = curl_init();

        $url = $this->rs_url.'/sbp/'.$this->api_version.'/customer/'.$this->customerCode;

        if ($this->bankCode != null)
            $url .= "/".$this->bankCode;

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$this->accesstoken_hybrid
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $customer = json_decode($response, true);

        $this->legalId = $customer['Data']['legalId'];
        $this->merchantId = $customer['Data']['MerchantList'][0]['merchantId'];
        $this->accountId = $customer['Data']['AccountList'][0]['accountId'];

        return $customer;

    }

    public function payment_check($uid, $data_raw, $webhookType) {

        $order = $this->db->query('SELECT * FROM orders WHERE uid = ? LIMIT 1', $uid)->fetchArray();

        $_GET['client_token'] = $data_raw['client_token'];
        $this->getNewToken();

        

        $curl = curl_init();

        if ($webhookType == "acquiringInternetPayment") {
            $data_response = json_decode($order["data_response"], true);
            $url = $data_response["Links"]["self"]. '/' .$uid;
    
            $isSBP = true;
            if (strpos($url, "acquiring") !== false)
                $isSBP = false;
        } else {
            $isSBP = true;

            if (isset($data_raw['client_token']['Data']['paymentMode']) && in_array("card", $data_raw['client_token']['Data']['paymentMode']))
                $isSBP = false;
    
            if ($isSBP)
                $url = $this->rs_url.'/sbp/'.$this->api_version.'/qr-codes/'.$uid.'/payment-status';
            else
                $url = $this->rs_url.'/acquiring/'.$this->api_version.'/payments/'.$uid;
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->accesstoken_hybrid
            ),
        ));

        $response = json_decode(curl_exec($curl), true);

    
        curl_close($curl);

        if ($isSBP) {
            if (isset($response['Data']) && isset($response['Data']['paymentList']) && count($response['Data']['paymentList']) > 0 && isset($response['Data']['paymentList'][0]['qrcId'])) {
                $status = false;
                if ($response['Data']['paymentList'][0]['status'] == "Accepted") {
                    $status = 2;
                } elseif (in_array($response['Data']['paymentList'][0]['status'], ["InProgress","Received"])) {
                    $status = 1;
                } elseif ($response['Data']['paymentList'][0]['status'] == "Rejected") {
                    $status = 4;
                }

                if ($status) {
                    $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', [$status, $response['Data']['paymentList'][0]['qrcId']]);
                    return ['uid' => $response['Data']['paymentList'][0]['qrcId'], 'status' => $status, 'time' => date('Y-m-d H:i:s')];
                } else {
                    return $response;
                }
                
            }
        } else {
            if (isset($response['Data']) && isset($response['Data']['Operation']) && count($response['Data']['Operation']) > 0 && isset($response['Data']['Operation'][0]['operationId'])) {
                $status = false;
                if ($response['Data']['Operation'][0]['status'] == "APPROVED") {
                    $status = 2;
                } elseif (in_array($response['Data']['Operation'][0]['status'], ["CREATED"])) {
                    $status = 1;
                } elseif ($response['Data']['Operation'][0]['status'] == "EXPIRED") {
                    $status = 4;
                }

                if ($status) {
                    $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', [$status, $response['Data']['Operation'][0]['operationId']]);
                    return ['uid' => $response['Data']['Operation'][0]['operationId'], 'status' => $status, 'time' => date('Y-m-d H:i:s')];
                } else {
                    return $response;
                }
                
            }
        }

        return $response;

    }

    public function createWebhook() {

        $curl = curl_init();

        $webhook = [
            "webhooksList" => ["incomingPayment", "incomingSbpPayment", "acquiringInternetPayment"],
            "url" => TOCHKA_DOMAIN."/api/v1/webhooks/tochka"
        ];

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->rs_url.'/webhook/'.$this->api_version.'/'.$this->client_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_POSTFIELDS => json_encode($webhook, JSON_UNESCAPED_UNICODE),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$this->accesstoken_hybrid,
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);
    }

    public function getWebhooks() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->rs_url.'/webhook/'.$this->api_version.'/'.$this->client_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer '.$this->accesstoken_hybrid,
                'Content-Type: application/json'
            ),
          ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);

    }

    private function getConsents() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->rs_url.'/'.$this->api_version.'/consents',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$this->accesstoken_hybrid,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);
    }

    private function deleteConsents($id) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->rs_url.'/'.$this->api_version.'/consents/'.$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'DELETE',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$this->accesstoken_hybrid,
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);
    }


    private function genUID($bankCode = null) {
        $uid = uniqid(NULL, TRUE);
        $rawid = strtoupper(sha1(uniqid(rand(), true)));
        $result = substr($uid, 6, 8);
        $result .= substr($uid, 0, 4);
        $result .= substr(sha1(substr($uid, 3, 3)), 0, 4);
        $result .= substr(sha1(substr(time(), 3, 4)), 0, 4);
        $result .= strtolower(substr($rawid, 10, 12));
        if (!is_null($bankCode))
            $result .= "-".$bankCode;
        return $result;
    }

    private function error($message, $payload = null)
    {
        return [
            "error" => [
                "message" => $message
            ],
            "payload" => $payload
        ];
    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }

}