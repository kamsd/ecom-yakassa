<?php

namespace Vendors;

/* Рассрочка Тинькофф */

class TinkoffCredit
{
	
    const BASE_URL = "https://forma.tinkoff.ru/api/partners/v2/orders";
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
        $this->shopId = "";
        $this->showcaseID = "";
        $this->password = "";
    }


    public function webhook_insert($headers, $body)
    {

        if (!array_key_exists('id', $body) || !array_key_exists('status', $body)) {
            $return['error'] = true;
            $return['message'] = 'Запрос Tinkoff Webhooks отклонен';
            return $return;
        }

        $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', [
            'tinkoff_credit',
            json_encode($headers, JSON_UNESCAPED_UNICODE),
            json_encode($body, JSON_UNESCAPED_UNICODE)
        ]);

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ?', $body['id']);

        if ($select->numRows() != 0) {
            $order = $select->fetchArray();
            $response = $this->db->query('UPDATE orders SET data_callback = ? WHERE external_uid = ?', array(json_encode($body, JSON_UNESCAPED_UNICODE), $body['id']));
        } else {
            $return['error'] = true;
            $return['message'] = 'Транзакция не найдена в БД (Tinkoff Webhooks)';
            return $return;
        }

        $data = json_decode($order['data_raw'],true);

        if ($body['status'] == 'signed') {
            $status = 1;
        } elseif ($body['status'] == 'approved') {
            $status = 2;
        } elseif ($body['status'] == 'canceled' || $body['status'] == 'rejected') {
            $status = 3;
        } else {
            $return['error'] = true;
            $return['message'] = 'Тип события Tinkoff Credit не поддерживается, запрос отклонен';
            return $return;
        }

        $this->db->query('UPDATE orders SET status = ? WHERE external_uid = ?', array($status, $body['id']));

        if (isset($data['callback_url']))
            (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));

        return [];
 
 
    }

    public function payment_new($data)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданным UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $response = [
            "shopId" => $this->shopId,
            "showcaseId" => $this->showcaseID,
            "sum" => floatval($data['payment']['amount']),
            "orderNumber" => $data['uid'],
            "items" => [
                [
                    "name" => $data['payment']['description'],
                    "quantity" => 1,
                    "price" => floatval($data['payment']['amount'])
                ]
            ],
            "orderNumber" => $data['uid'],
            "successURL" => $data['return_url'],
            "promoCode" => $data['promoCode'] ?? "default"
            //"webhookURL" => $data['callback_url']
        ];

        $output = $this->api("/create", "POST", $response);
        
        if (!array_key_exists("link", $output)) {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при создании платежа';
            $return['data'] = $output;
            return $return;
        }

        $insert = $this->db->query(
            'INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)',
            [
                $output['id'],
                $data['uid'],
                'tinkoff_credit',
                json_encode($data),
                json_encode($output),
                0
            ]
        );

        if ($insert->affectedRows() == 1) {

            return array(
                'payment_url' => $output['link']
            );

        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }



    public function payment_check($paymentId, $data_raw) {
        
        $data = array();
        $data['TerminalKey'] = $data_raw['tokens']['TerminalKey'];
        $data['Password'] = $data_raw['tokens']['Password'];
        $data['PaymentId'] = $paymentId;

        $response = $this->post("GetState", $data);
 
        $return = array();

        if ($response['ErrorCode'] == '0'){
            
            if ($response['Status'] == 'AUTHORIZED') {
                $status = 1;
            } elseif ($response['Status'] == 'CONFIRMED') {
                $status = 2;
            } elseif ( ($response['Status'] == 'REJECTED') or ($response['Status'] == 'REVERSED') ) {
                $status = 3;
            }

            if (isset($status)) {
                $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['PaymentId']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $return['error'] = true;
                    return $return;
                }
            } else {
                $return['error'] = true;
                return $return;
            }

        } else {
            $return['error'] = true;
            return $return;
        }

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу Тинькофф';
            return $return;
        }

        if ( (!array_key_exists('showcaseID', $data['tokens'])) and (!array_key_exists('password', $data['tokens'])) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу Тинькофф';
            return $return;
        }

        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        /*
        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }
        */

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    
    
    private function api($url, $method = "GET", $data = ""){
                                                                                                                                                                              
        $ch = curl_init(self::BASE_URL . $url);
        
        if ($method == "POST") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));  
        }
        curl_setopt($ch, CURLOPT_USERPWD, $this->showcaseID . ":" . $this->password);                                                          
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json'                                                                    
        ));

        $result = curl_exec($ch);
        
        if (strpos($url, "/info") !== false) { 
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            return [
                "ErrorCode" => $httpcode == 404 ? 8 : 1
            ];
        }
        return json_decode($result, true);
    }


    public function check($data){

        if ( (!array_key_exists('showcaseID', $data)) || (!array_key_exists('password', $data)) || (!array_key_exists('shopId', $data)) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи для подключения к терминалу рассрочки Тинькофф';
            return $return;
        }

        $this->shopId = $data["shopId"];
        $this->showcaseID = $data["showcaseID"];
        $this->password = $data["password"];

        $response = $this->api("/0/info");

        $return = [];

        if ($response['ErrorCode'] != '8'){
            $return['error'] = true;
            $return['message'] = 'Ключ доступа к терминалу Тинькофф не действителен';
            return $return;
        } else {
            $return['message'] = 'Ключ доступа к терминалу Тинькофф действителен';
            return $return;
        }
     
    }

}


