<?php

namespace Vendors;

/* Интернет-эквайринг Тинкофф */

class Tinkoff
{
	
	public function __construct($db = null, $user_id = null)
	{
        $this->db = is_null($db) ? new \Ecomkassa\DB() : $db;
        $this->TerminalKey = false;
        $this->Password = false;
        $this->user_id = $user_id;
    }


    public function webhook_insert($headers, $body)
    {

        $RebillId = null;
        
        if ( (array_key_exists('OrderId', $body)) and (array_key_exists('PaymentId', $body)) ) {

            if (array_key_exists('RebillId', $body))
                $RebillId = $body['RebillId'];

            $response = $this->db->query('UPDATE ' . ($RebillId ? 'autopayments' : 'orders') . ' SET data_callback = ? WHERE uid = ?',
                [
                    json_encode($body, JSON_UNESCAPED_UNICODE),
                    (string)$body['PaymentId']]
            );

            $insert = $this->db->query('INSERT INTO webhooks (source, data_header, data_body) VALUES (?,?,?)', ['tinkoff',json_encode($headers, JSON_UNESCAPED_UNICODE), json_encode($body, JSON_UNESCAPED_UNICODE)]);
            $num_rows = $insert->affectedRows();

        } else {
            $return['error'] = true;
            $return['message'] = 'Запрос Tinkoff Webhooks отклонен';
            return $return;
        }
        
        if ($num_rows == 1) {

            if (array_key_exists('Status', $body)) {

                $select = $this->db->query('SELECT * FROM ' . ($RebillId ? 'autopayments' : 'orders') . ' WHERE uid = ?', $body['PaymentId']);

                if ($select->numRows() != 0) {
                    $order = $select->fetchArray();
                } else {
                    $return['error'] = true;
                    $return['message'] = 'Транзакция не найдена в БД (Tinkoff Webhooks)';
                    return $return;
                }

                $data = json_decode($order['data_raw'],true);


                if ($body['Status'] == 'AUTHORIZED') {
                    $status = 1;
                } elseif ($body['Status'] == 'CONFIRMED') {
                    $status = 2;
                } elseif ( ($body['Status'] == 'REJECTED') or ($body['Status'] == 'REVERSED') ) {
                    $status = 3;
                } else {

                    $return['message'] = 'Тип события Tinkoff не поддерживается, запрос отклонен';
                    return $return;
                }

                if ($RebillId)
                    $this->db->query('UPDATE autopayments SET status = ?, rebillId = ? WHERE uid = ?', array($status, $RebillId, (string)$body['PaymentId']));
                else
                    $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, (string)$body['PaymentId']));

                if (array_key_exists("callback_url", $data))
                    (new \Redirect())->redirect($data['callback_url'], array('uid' => $order['external_uid'], 'status' => $status, 'time' => date('Y-m-d H:i:s')));
                
            }                    

        }
 
    }

    public function payment_new($data, $recurrent = false)
    {

        $return = array();

        $check = $this->check_payment_data($data);

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM orders WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданным UID уже существует';
            return $return;
        }

        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $response = array();
        $response['TerminalKey'] = $data['tokens']['TerminalKey'];
        $response['Password'] = $data['tokens']['Password'];
        $response['Amount'] = floatval($data['payment']['amount']) * 100;
        $response['OrderId'] = $data['uid'];
        $response['Description'] = $data['payment']['description'];

        if (isset($_SERVER['HTTPS']))
            $scheme = $_SERVER['HTTPS'];
        else
            $scheme = '';
        if (($scheme) && ($scheme != 'off')) $scheme = 'https';
        else $scheme = 'http';

        //$response['NotificationURL'] = $scheme.'://'.$_SERVER['SERVER_NAME'].'/api/v1/webhooks/tinkoff';
        $response['SuccessURL'] = $data['return_url'];
        $output = $this->post("Init", $response);
        
        if ((isset($output['Success'])) and (!$output['Success'])) {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при создании платежа';
            $return['data'] = $output;
            return $return;
        }

        $sql_vars = array(
            $output['PaymentId'],
            $data['uid'],
            'tinkoff',
            json_encode($data),
            json_encode($output),
            0
        );

        $insert = $this->db->query('INSERT INTO orders (uid, external_uid, source, data_raw, data_response, status) VALUES (?,?,?,?,?,?)', $sql_vars);

        if ($insert->affectedRows() == 1) {

            if (isset($data['tokens']['payment_type'])) {
                if ($data['tokens']['payment_type'] == "sbp") {
                    //$SBPParser = (new \SBPParser())->get(["vendor" => "tinkoff", "url" => $output['PaymentURL']]);
                    $SBPParser = $this->GetQr([
                        "TerminalKey" => $data['tokens']['TerminalKey'],
                        "Password" => $data['tokens']['Password'],
                        "PaymentId" => $output['PaymentId']
                    ]);
                    if (isset($SBPParser['Data']))
                        return array(
                            'payment_url' => str_replace("https://qr.nspk.ru/", "https://web.qr.nspk.ru/", $SBPParser['Data'])
                        ); 
                    else return $SBPParser;

                } elseif ($data['tokens']['payment_type'] == "acquiring") {
                    return array(
                        'payment_url' => $output['PaymentURL']
                    ); 
                } else {
                    return [
                        "error" => true,
                        "message" => "Параметр payment_type имеет недопустимое значение"
                    ];
                }

            } else {
                return array(
                    'payment_url' => $output['PaymentURL']
                );
            }

            
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }

    private function GetQr($data) {
        return $this->post("GetQr", $data);
    }

    public function payment_check($paymentId, $data_raw) {
        
        $data = array();
        $data['TerminalKey'] = $data_raw['tokens']['TerminalKey'];
        $data['Password'] = $data_raw['tokens']['Password'];
        $data['PaymentId'] = $paymentId;

        $response = $this->post("GetState", $data);
 
        $return = array();

        if ($response['ErrorCode'] == '0'){
            
            if ($response['Status'] == 'AUTHORIZED') {
                $status = 1;
            } elseif ($response['Status'] == 'CONFIRMED') {
                $status = 2;
            } elseif ( ($response['Status'] == 'REJECTED') or ($response['Status'] == 'REVERSED') ) {
                $status = 3;
            }

            if (isset($status)) {
                $update = $this->db->query('UPDATE orders SET status = ? WHERE uid = ?', array($status, $response['PaymentId']));
                if ($update->affectedRows() == 1) {
                    return array(
                        'status' => $status,
                        'updated' => date('Y-m-d H:i:s')
                    );
                } else {
                    $return['error'] = true;
                    return $return;
                }
            } else {
                $return['error'] = true;
                return $return;
            }

        } else {
            $return['error'] = true;
            return $return;
        }

    }

    
    private function check_payment_data($data) {
        
        if (!array_key_exists('payment', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует информация о платеже';
            return $return;
        }

        if (!array_key_exists('uid', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует id платежа';
            return $return;
        }

        if (!array_key_exists('amount', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует сумма платежа';
            return $return;
        }

        if (!array_key_exists('description', $data['payment'])) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует наименование платежа';
            return $return;
        }

        if (!array_key_exists('tokens', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Тинькофф';
            return $return;
        }

        if ( (!array_key_exists('TerminalKey', $data['tokens'])) and (!array_key_exists('Password', $data['tokens'])) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи авторизации для подключения к терминалу интернет-эквайринга Тинькофф';
            return $return;
        }

        /*
        if (!array_key_exists('callback_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для callback-запроса';
            return $return;
        }

        if (!array_key_exists('return_url', $data)) {
            $return['error'] = true;
            $return['message'] = 'Отстутствует url для возврата со страницы оплаты';
            return $return;
        }
        */

        return array();

    }

    private function redirect($url, $data) {
		$ch = curl_init();
		$data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        curl_close($ch);
		return $result;
    }
    
    private function post($method,$data){
        $data = $this->addCreditionals($data);
        unset($data['Password']);
        $data_string = json_encode($data);   
        //var_dump('https://securepay.tinkoff.ru/v2/' . $method); 
        //var_dump($data_string);                                                                                                                                                                                                   
        $ch = curl_init('https://securepay.tinkoff.ru/v2/' . $method);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );

        $result = curl_exec($ch);
       // var_dump($result); 
        return json_decode($result, true);
    }

    private function addCreditionals($data){
        
        ksort($data);
        
        $str = '';

        foreach($data as $d){
            $str .= $d;
        }
        
        $data['Token'] = hash('sha256', $str);
        return $data;
    }

    public function check($data){

        unset($data['payment_type']);

        if ( (!array_key_exists('TerminalKey', $data)) and (!array_key_exists('Password', $data)) ) {
            $return['error'] = true;
            $return['message'] = 'Отстутствуют ключи для подключения к терминалу интернет-эквайринга Тинькофф';
            return $return;
        }

        $data['PaymentId'] = "999999";

        $response = $this->post("GetState", $data);

        $return = array();

        if ($response['ErrorCode'] != '8'){
            $return['error'] = true;
            $return['message'] = 'Ключ доступа к терминалу интернет-эквайринга Тинькофф не действителен';
            return $return;
        } else {
            $return['message'] = 'Ключ доступа к терминалу интернет-эквайринга Тинькофф действителен';
            return $return;
        }
     
    }



    public function autopayment_new($data)
    {

        $return = array();

        if (!array_key_exists('payment_system', $data))
            return [
                "error" => true,
                "message" => "Не указан payment_system"
            ];

        if (!is_int($data['payment_system']))
            return [
                "error" => true,
                "message" => "Некорректное значение payment_system"
            ];


        $PaymentSystems = new \Ecomkassa\PaymentSystems($this->db, $this->user_id, $data['payment_system']);

        $ps_data = $PaymentSystems->get();

        if (array_key_exists('error', $ps_data))
            return $ps_data;

        $data['tokens'] = $ps_data['tokens'];
        

        $check = $this->check_payment_data($data);

      

        if (array_key_exists('error', $check)) return $check;

        $select = $this->db->query('SELECT * FROM autopayments WHERE external_uid = ? LIMIT 1', $data['uid']);

        if ($select->numRows() != 0) {
            $return['error'] = true;
            $return['message'] = 'Платеж с заданным UID уже существует';
            return $return;
        }
   
        $creditionals = $this->check($data['tokens']);

        if (array_key_exists('error', $creditionals)) {
            $return['error'] = true;
            $return['message'] = 'Учетные данные для подключения заданы неверно';
            return $return;
        }

        $Customers = new \Ecomkassa\Customers($this->db, $this->user_id);

        $RebillId = null;

        if (isset($data['client']['id'])) {
            $customer = $Customers->findById($data['client']['id']);
            if (count($customer) == 0)
                return [
                    "error" => true,
                    "message" => "Пользователь не найден"
                ];

            $select = $this->db->query('SELECT rebillId FROM autopayments WHERE customer_id = ? and source = ? and recurrent = ? and payment_system = ? LIMIT 1', [
                $data['client']['id'],
                "tinkoff",
                1,
                $data['payment_system']
            ]);

            if ($select->numRows() == 0)
                return [
                    "error" => true,
                    "message" => "Рекурентный платеж не найден"
                ];

            $RebillId = $select->fetchArray()["rebillId"];


        } else
            $customer = $Customers->add(["email" => $data['client']['email'] ?? null, "phone" => $data['client']['phone'] ?? null]);
        

        if (array_key_exists('error', $customer))
            return $customer;

        $response = array();
        $response['TerminalKey'] = $data['tokens']['TerminalKey'];
        $response['Password'] = $data['tokens']['Password'];
        $response['Amount'] = floatval($data['payment']['amount']) * 100;
        $response['OrderId'] = $data['uid'];
        $response['Description'] = $data['payment']['description'];

        if (!isset($data['client']['id'])) {
            $response['Recurrent'] = "Y";
            $response['CustomerKey'] = strval($customer["id"]);
        }
    
        if (isset($_SERVER['HTTPS']))
            $scheme = $_SERVER['HTTPS'];
        else
            $scheme = '';
        if (($scheme) && ($scheme != 'off')) $scheme = 'https';
        else $scheme = 'http';

        //$response['NotificationURL'] = $scheme.'://'.$_SERVER['SERVER_NAME'].'/api/v1/webhooks/tinkoff';
        
        if (isset($data['return_url']))
            $response['SuccessURL'] = $data['return_url'];
        
        $output = $this->post("Init", $response);
        
        if ((isset($output['Success'])) and (!$output['Success'])) {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при создании платежа';
            $return['data'] = $output;
            return $return;
        }

        $sql_vars = array(
            $output['PaymentId'],
            $this->user_id,
            $data['uid'],
            !isset($data['client']['id']) ? 1 : 0,
            $customer["id"],
            $data['payment_system'],
            'tinkoff',
            json_encode($data),
            json_encode($output),
            $RebillId,
            0
        );

 
        $insert = $this->db->query('INSERT INTO autopayments (uid, user_id, external_uid, recurrent, customer_id, payment_system, source, data_raw, data_response, rebillId, status) VALUES (?,?,?,?,?,?,?,?,?,?,?)', $sql_vars);

        if ($insert->affectedRows() == 1) {

            if (!isset($data['client']['id'])) {

                if (isset($data['tokens']['payment_type'])) {
                    if ($data['tokens']['payment_type'] == "sbp") {
                        //$SBPParser = (new \SBPParser())->get(["vendor" => "tinkoff", "url" => $output['PaymentURL']]);
                        $SBPParser = $this->GetQr([
                            "TerminalKey" => $data['tokens']['TerminalKey'],
                            "Password" => $data['tokens']['Password'],
                            "PaymentId" => $output['PaymentId']
                        ]);
                        if (isset($SBPParser['Data']))
                            return array(
                                'payment_url' => str_replace("https://qr.nspk.ru/", "https://web.qr.nspk.ru/", $SBPParser['Data'])
                            ); 
                        else return $SBPParser;
    
                    } elseif ($data['tokens']['payment_type'] == "acquiring") {
                        return array(
                            'payment_url' => $output['PaymentURL']
                        ); 
                    } else {
                        return [
                            "error" => true,
                            "message" => "Параметр payment_type имеет недопустимое значение"
                        ];
                    }
    
                } else {
                    return array(
                        'payment_url' => $output['PaymentURL']
                    );
                }

            } else {
                $charge = $this->post("Charge", [
                    'TerminalKey' => $response['TerminalKey'],
                    'Password' => $response['Password'],
                    "RebillId" => $RebillId,
                    "PaymentId" => $output['PaymentId']
                ]);
                return $charge;
            }

            
        }
        else  {
            $return['error'] = true;
            $return['message'] = 'Произошла ошибка при записи данных в БД';
            return $return;
        } 

        return $response;

    }




}


