<?php


class Redirect
{
	
	public function __construct()
	{
        $this->db = new \Ecomkassa\DB();
    }

    public function redirect($callback_url, $data) {

        if (strpos($callback_url, ';') !== false) {
            foreach (explode(";", $callback_url) as $url) {
                if (strpos($url, 'https://forms.tildacdn.com/payment/') !== false && in_array($data["status"], [1,2])) {
                    $r = (new \Modules\Tilda())->sendSuccess($url, $data['uid']);
                }
                else
                    $this->send($url, $data);
            }

        } else {
            $this->send($callback_url, $data);
        }

        return;
        	
    }

    private function send($url, $data) {

		$data_string = json_encode($data);

        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
        ];
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
      
        $this->log([
            "external_uid" => array_key_exists("uid", $data) ? $data["uid"] : null,
            "url" => $url,
            "header" => json_encode($headers, JSON_UNESCAPED_UNICODE),
            "body" => $data_string,
            "httpcode" => curl_getinfo($ch, CURLINFO_HTTP_CODE),
            "response" => $result
        ]);

        curl_close($ch);

		return $result;
    }

    private function log($data) {
        $this->db->query('INSERT INTO redirects_log 
            (url, header, body, httpcode, response) 
            VALUES (?,?,?,?,?)',
            [
                $data['url'],
                $data['header'],
                $data['body'],
                $data['httpcode'],
                $data['response']
            ]
        );
    }

}