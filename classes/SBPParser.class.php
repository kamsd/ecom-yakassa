<?php

/* Парсер ссылок СБП */

class SBPParser
{
	
	public function __construct()
	{
        $this->api_url = "http://84.201.148.174:8000/api/v1";
    }

    public function get($data) {
        if (!isset($data['vendor'])) {
            return [
                "error" => true,
                "message" => "Не указан vendor"
            ];
        }

        if (!in_array($data['vendor'], ["tinkoff"]))
            return [
                "error" => true,
                "message" => "Vendor ".$data['vendor']." не поддерживается"
            ];

        $result = $this->post("/sbp", $data);
        if ($result == null)
            return [
                "error" => true,
                "message" => "Сервис обработки ссылок СБП недоступен"
            ];
        else if (isset($result['error']))
            return [
                "error" => true,
                "message" => $result['error']
            ];
        else return $result;
    }

    private function post($method,$data) {     
        $data_string = json_encode($data);                                                                                                                                                                           
        $ch = curl_init($this->api_url . $method);                                                             
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json; charset=utf-8',                                                                                
            'Content-Length: ' . strlen($data_string)                                                            
        ));

        $result = curl_exec($ch);
        return json_decode($result, true);
    }

}
