<?php

// Приемник API-запросов

if (!function_exists('getallheaders')) {
    function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
    }
}

$headers = getallheaders();


function getBearerToken() {
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return false;
}

$bearer = getBearerToken();
$DB = $bearer ? new \Ecomkassa\DB() : null;


$return = array();
$error = array();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $response = array();

    if (in_array($name, ["customers", "autopayments", "payment_systems"])) {
        $Users = new \Ecomkassa\Users($DB);
        $user = $Users->getUserByToken($bearer);
        if (!$user) {
            header("HTTP/1.1 401 Unauthorized");
            exit();
        }
            
    }

    if (isset($name)) {
        
        if ($name == 'payments') {
            if ($parameter === NULL) {
                $error['code'] = 404;
                $error['message'] = 'Не задан идентификатор платежа';
                $response['error'] = $error;
            } else {
                $response = (new \Ecomkassa\Payments())->getInfo($parameter);
            }
            
        } elseif ($name == 'webhooks') {
            if ($parameter == 'sberbank') {
                $response = (new \Vendors\Sberbank())->webhook_insert($headers, $_GET);
            } else if ($parameter == 'alfabank') {
                $response = (new \Vendors\Alfabank())->webhook_insert($headers, $_GET);
            } else {
                $error['code'] = 404;
                $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                $response['error'] = $error;
            }
        } elseif ($name == 'tochka') {
            if ($parameter == 'link') {
                $response = (new \Vendors\Tochka())->getLink();
            } elseif ($parameter == 'change_token') {
                $response = (new \Vendors\Tochka())->changeCodeToToken();
            } elseif ($parameter == 'new_token') {
                $response = (new \Vendors\Tochka())->getNewToken();
            } elseif ($parameter == 'customers') {
                $response = (new \Vendors\Tochka())->getCustomer();
            } else {
                $error['code'] = 404;
                $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                $response['error'] = $error;
            }
        } elseif ($name == 'customers') {
            $response = (new \Ecomkassa\Customers($DB, $user["user_id"]))->list(); 
        } elseif ($name == 'payment_systems') {
            if ($parameter === NULL)
                $response = (new \Ecomkassa\PaymentSystems($DB, $user['user_id']))->list(); 
            else
                $response = (new \Ecomkassa\PaymentSystems($DB, $user["user_id"], $parameter))->get(); 
        } elseif ($name == 'autopayments') {
            if ($parameter === NULL)
                $response = (new \Ecomkassa\Autopayments($DB, $user["user_id"]))->list(); 
            else
                $response = (new \Ecomkassa\Autopayments($DB, $user["user_id"]))->get($parameter); 

        }  else {
            $error['code'] = 404;
            $error['message'] = 'Запрос статуса с заданными параметрами не поддерживается';
            $response['error'] = $error;
        }
  
    } else {
        $error['code'] = 404;
        $error['message'] = 'Метод не установлен';
        $response['error'] = $error;
    }

    $return = $response;
  
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $response = array();
    $raw_post_data = file_get_contents('php://input');
    json_decode($raw_post_data);

    if (json_last_error() == JSON_ERROR_NONE || ($name == 'webhooks' && in_array($parameter, ['tochka', 'paygine']))) {

        if (in_array($name, ["payment_systems", "autopayments"])) {
            $Users = new \Ecomkassa\Users($DB);
            $user = $Users->getUserByToken($bearer);
            if (!$user) {
                header("HTTP/1.1 401 Unauthorized");
                exit();
            }
                
        }

        
        
        if (($name != 'webhooks' || !in_array($parameter, ['tochka', 'paygine'])))
            $data = json_decode($raw_post_data, true);
        if (isset($name)) {
            if ($name == 'webhooks') {   
                if ($parameter == 'yandex') {
                    $response = (new \Vendors\Yandex())->webhook_insert($headers, $data);
                } elseif ($parameter == 'tinkoff') {
                    $response = (new \Vendors\Tinkoff())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'tinkoff_credit') {
                    $response = (new \Vendors\TinkoffCredit())->webhook_insert($headers, $data);
                    if (!isset($response["error"]))
                        die('OK');
                } elseif ($parameter == 'rbkmoney') {
                    $response = (new \Vendors\RBKMoney())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'invoice') {
                    $response = (new \Vendors\Invoice())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'ecomkassa') {
                    $response = (new \Vendors\Invoice())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'robokassa') {
                    $response = (new \Vendors\Robokassa())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'tochka') {
                    $response = (new \Vendors\Tochka())->webhook_insert($headers, $raw_post_data);
                    die('OK');
                } elseif ($parameter == 'raiffeisen') {
                    $response = (new \Vendors\Raiffeisen())->webhook_insert($headers, $data);
                    die('OK');
                } elseif ($parameter == 'tilda') {
                    $response = (new \Modules\Tilda())->webhook_insert($data);
                    die('OK');
                } elseif ($parameter == 'paygine') {
                    $response = (new \Vendors\Paygine())->webhook_insert($raw_post_data);
                    //die('OK');
                } elseif ($parameter == 'yourpayments_podeli') {
                    echo json_encode((new \Vendors\YourPaymentsPodeli())->webhook_insert($headers, $data), JSON_UNESCAPED_UNICODE);
                    die();
                } else {
                    $error['code'] = 404;
                    $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                    $response['error'] = $error;
                }      
            } elseif ($name == 'check') {   
                if ($parameter == 'yandex') {
                    $response = (new \Vendors\Yandex())->check($data);
                } elseif ($parameter == 'tinkoff') {
                    $response = (new \Vendors\Tinkoff())->check($data);
                } elseif ($parameter == 'sberbank') {
                    $response = (new \Vendors\Sberbank())->check($data);
                } elseif ($parameter == 'rbkmoney') {
                    $response = (new \Vendors\RBKMoney())->check($data);
                } elseif ($parameter == 'invoice') {
                    $response = (new \Vendors\Invoice())->check($data);
                } elseif ($parameter == 'ecomkassa') {
                    $response = (new \Vendors\Invoice())->check($data);
                } elseif ($parameter == 'alfabank') {
                    $response = (new \Vendors\Alfabank())->check($data);
                } elseif ($parameter == 'raiffeisen') {
                    $response = (new \Vendors\Raiffeisen())->check($data);
                } elseif ($parameter == 'tinkoff_credit') {
                    $response = (new \Vendors\TinkoffCredit())->check($data);
                } elseif ($parameter == 'paygine') {
                    $response = (new \Vendors\Paygine())->check($data);
                } else {
                    $error['code'] = 404;
                    $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                    $response['error'] = $error;
                }      
            } elseif ($name == 'payments') {

                if ($parameter == 'yandex') {
                    $response = (new \Vendors\Yandex())->payment_new($data);
                } elseif ($parameter == 'tinkoff') {
                    $response = (new \Vendors\Tinkoff())->payment_new($data);
                } elseif ($parameter == 'sberbank') {
                    $response = (new \Vendors\Sberbank())->payment_new($data);
                } elseif ($parameter == 'rbkmoney') {
                    $response = (new \Vendors\RBKMoney())->payment_new($data);
                } elseif ($parameter == 'invoice') {
                    $response = (new \Vendors\Invoice())->payment_new($data);
                } elseif ($parameter == 'ecomkassa') {
                    $response = (new \Vendors\Invoice())->payment_new($data);
                } elseif ($parameter == 'robokassa') {
                    $response = (new \Vendors\Robokassa())->payment_new($data);
                } elseif ($parameter == 'alfabank') {
                    $response = (new \Vendors\Alfabank())->payment_new($data);
                } elseif ($parameter == 'tochka') {
                    $response = (new \Vendors\Tochka())->payment_new($data);
                } elseif ($parameter == 'raiffeisen') {
                    $response = (new \Vendors\Raiffeisen())->payment_new($data);
                } elseif ($parameter == 'tinkoff_credit') {
                    $response = (new \Vendors\TinkoffCredit())->payment_new($data);
                } elseif ($parameter == 'yourpayments_podeli') {
                    $response = (new \Vendors\YourPaymentsPodeli())->payment_new($data);
                } elseif ($parameter == 'paygine') {
                    $response = (new \Vendors\Paygine())->payment_new($data);
                } else {
                    $error['code'] = 404;
                    $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                    $response['error'] = $error;
                }   
                
            } elseif ($name == 'users') {

                if ($parameter == 'registration') {
                    $response = (new \Ecomkassa\Users())->registration($data);
                } elseif ($parameter == 'auth') {
                    $response = (new \Ecomkassa\Users())->auth($data);
                } else {
                    $error['code'] = 404;
                    $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                    $response['error'] = $error;
                }

            } elseif ($name == 'payment_systems') {
                $response = (new \Ecomkassa\PaymentSystems($DB, $user['user_id']))->add($data);
            } elseif ($name == 'autopayments') {
                if ($parameter == 'tinkoff') {
                    $response = (new \Vendors\Tinkoff($DB, $user['user_id']))->autopayment_new($data);
                } else {
                    $error['code'] = 404;
                    $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                    $response['error'] = $error;
                } 
                
            } else {
                $error['code'] = 404;
                $error['message'] = 'Запрос статуса с заданными параметрами не поддерживается';
                $response['error'] = $error;
            }

        } else {
            $error['code'] = 404;
            $error['message'] = 'Метод не установлен';
            $response['error'] = $error;
        }

         $return = $response;


    } elseif ($name == "modules" && isset($parameter)) {
        if ($parameter == "tilda") {
            $response = (new \Modules\Tilda())->getLink($raw_post_data);
            if (isset($response['link'])) {
                //var_dump($response['link']);
                header("Location: ".$response['link']);
            } else {
                echo json_encode($response, JSON_UNESCAPED_UNICODE, JSON_PRETTY_PRINT);
            }
            die();
        } else {
            $error['code'] = 404;
            $error['message'] = 'Запрос с заданными параметрами не поддерживается';
            $response['error'] = $error;
        }
        $return = $response;
    } else {
        $error['code'] = 404;
        $error['message'] = 'Запрос не в формате JSON';
        $return['error'] = $error;
    }

} elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    $Users = new \Ecomkassa\Users($DB);
    $user = $Users->getUserByToken($bearer);
    if (!$user) {
        header("HTTP/1.1 401 Unauthorized");
        exit();
    }

    $raw_post_data = file_get_contents('php://input');
    json_decode($raw_post_data);

    
    if (json_last_error() == JSON_ERROR_NONE) {

        $data = json_decode($raw_post_data, true);

        if (isset($name)) {

            if ($name == 'payment_systems' && isset($parameter)) {
                $return = (new \Ecomkassa\PaymentSystems($DB, $user['user_id'], $parameter))->update($data);
            }  else {
                $error['code'] = 404;
                $error['message'] = 'Запрос с заданными параметрами не поддерживается';
                $response['error'] = $error;
            }

        } else {
            $error['code'] = 404;
            $error['message'] = 'Запрос с заданными параметрами не поддерживается';
            $response['error'] = $error;
        }
    } else {
        $error['code'] = 404;
        $error['message'] = 'Запрос не в формате JSON';
        $return['error'] = $error;
    }
}

if (isset($return['error']['code'])) {
    $code = $return['error']['code'];
    if ($code == 404) {
        header("HTTP/1.1 404 Not Found");
    } elseif ($code == 204) {
        header("HTTP/1.0 204 No Response");
    }
    
    $return['message'] = $return['error']['message'];
    $return['error'] = true;
}

echo json_encode($return, JSON_UNESCAPED_UNICODE, JSON_PRETTY_PRINT);
exit();